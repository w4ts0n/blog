---
name: "Neue Podcast-Episode"
about: "Mit diesem Template lassen sich Ideen für Podcast-Episoden dokumentieren."
title: "[Podcast] Neue Podcast-Episode mit folgendem Titel ''"
ref: "main"
labels:
  - "Typ: Podcast"
  - "Status: Draft"
---

## Titel / Title

Der Titel der Podcast-Episode soll wie folgt lauten:

## Autorenschaft / Authorship

Der Podcast-Host ist .... .

## Zusammenfassung / Summary

Die Zusammenfassung der Podcast-Episode lautet:

## Geplantes Datum der Veröffentlichung / Planned date of publication

Das geplantes Datum der Veröffentlichung ist der dd.mm.yyyy

## Kategorie / Category

Dieser Podcast-Episode sollen folgende Kategorien zugeordet werden:

- App-Check
- Artikleserie
- Kommentar
- Erfahrungsbericht
- Gastbeitrag
- Interview
- Website-Check
- JPA-Nachweis

## Schlüsselwörter / Keywords

Dieser Podcast-Episode soll folgende Schlüsselwörter zugeordet werden:

- ...
- ...
- ...

## Nachweis journalistischer

Diese Podcast-Episode wird als Nachweis journalistischer Tätigkeit eingereicht.

## Zusätzlicher Kontext
