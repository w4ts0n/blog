---
name: "Neue Podcast-Episode"
about: "Mit diesem Template lassen sich Ideen für Podcast-Episoden dokumentieren."
title: "[Podcast] Neuer Beitrag mit folgendem Titel ''"
ref: "main"
labels:
  - podcast
  - draft
---

## Titel / Title

## Autorenschaft / Authorship

## Zusammenfassung / Summary

## Geplantes Datum der Veröffentlichung / Planned date of publication

## Kategorie / Category

## Tags / Keywords

## Gäste (optional)

## Sonstiges
