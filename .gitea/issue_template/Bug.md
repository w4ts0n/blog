---
name: "Neuer Fehler"
about: "Hiermit kann ein auftretender Fehler im Blog oder Podcast gemeldet werden."
title: "[BUG]"
ref: "main"
labels:
  - bug
  - "help needed"
---

## Zusammenfassung

## Aktuelles Verhalten

## Zu erwartendes Verhalten

## Lösung

## Screenshots
