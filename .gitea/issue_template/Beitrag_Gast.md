---
name: "Neuer Gastbeitrag im Blog"
about: "Mit diesem Template lassen sich Gastbeiträge für Blogbeiträge einreichen."
title: "[Blog] Neuer Gastbeitrag im Blog mit folgendem Titel ''"
ref: "main"
labels:
  - blog
  - draft
---

## Titel / Title

## Autorenschaft / Authorship

## Zusammenfassung / Summary

## Geplantes Datum der Veröffentlichung / Planned date of publication

## Kategorie / Category

## Tags / Keywords

## Sonstiges
