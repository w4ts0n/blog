---
name: "Neuer Gastbeitrag im Blog"
about: "Mit diesem Template lassen sich Ideen für Blogbeiträge einreichen."
title: "[Blog] Neuer Gastbeitrag im Blog mit folgendem Titel ''"
ref: "main"
labels:
  - "Typ: Blog"
  - "Status: Draft"
---

## Titel / Title

Der Titel des Beitrags soll wie folgt lauten:

## Autorenschaft / Authorship

Der Beitragschreibende Person ist .... .

## Zusammenfassung / Summary

Die Zusammenfassung des Beitrags lautet:

## Geplantes Datum der Veröffentlichung / Planned date of publication

Das geplantes Datum der Veröffentlichung ist der dd.mm.yyyy

## Kategorie / Category

Dem Beitrag sollen folgenden Kategorien zugeordet werden:

- App-Check
- Artikleserie
- Kommentar
- Erfahrungsbericht
- Gastbeitrag
- Interview
- Website-Check
- JPA-Nachweis

## Schlüsselwörter / Keywords

Dem Beitrag soll folgenden Schlüsselwörter zugeordet werden:

- ...
- ...
- ...

## Nachweis journalistischer

Dieser Beitrag wird als Nachweis journalistischer Tätigkeit eingereicht.

## Zusätzlicher Kontext
