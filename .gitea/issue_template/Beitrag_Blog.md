---
name: "Neuer Blogbeitrag"
about: "Mit diesem Template lassen sich Ideen für Blogbeiträge dokumentieren."
title: "[Blog] Neuer Beitrag mit folgendem Titel ''"
ref: "main"
labels:
  - blog
  - draft
---

## Titel / Title

## Autorenschaft / Authorship

## Zusammenfassung / Summary

## Geplantes Datum der Veröffentlichung / Planned date of publication

## Kategorie / Category

## Tags / Keywords

## Sonstiges
