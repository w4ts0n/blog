---
title: "Transparenzseite"
author: "Lukas Schieren"
date: "2022-07-14"
lastmod: "2023-02-01"
categories: []
tags: []
weight: 2
draft: false
hidden: true
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
---

# Richtigstellungen von in Blogbeiträgen erwähnten Thesen, Behauptungen, die sich nachträglich als nicht wahr erwiesen haben

**Aktuell sind null Einträge in der Liste vorhanden.**

# Liste

| Titel des Beitrages | Veröffentlicht am | Zuletzt aktualisiert am |
| :------------------ | :---------------- | :---------------------- |

---

# Nachweise der journalistischen Tätigkeit zum Erwerb des Jugendpresse-Ausweises (JPA)

Generell Informationen zum JPA habe ich in [diesem Beitrag]({{< ref "jpa" >}}) zusammengefasst.

- [2022]({{< ref "jpa/2022" >}})
- [2023]({{< ref "jpa/2023" >}})
- [2024]({{< ref "jpa/2024" >}})

# Mitgliedschaften

Hier dokumentiere ich meine [Mitgliedschaften]({{< ref "memberships" >}}).

# Icons in der Socials Leiste

In der oberen rechten Ecke, oberhalb der Navigationsleiste, befinden sich Icons für E-Mail, Mastodon, Codeberg, Matrix und den RSS-Feed.

Die E-Mail-, Mastodon-, Codeberg- und das RSS-Feed-Icon sind Teil der Font Awesome Free und stehen unter der [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).

Weitere Informationen hierzu unter folgendem Link: https://fontawesome.com/license/free

Das Matrix-Icon wird vom matrix.org-Projekt unter https://github.com/vector-im/logos/tree/master/matrix bereitgestellt.
