---
title: "Mitgliedschaften & Vereinsfunktionen"
author: "Lukas Schieren"
date: "2023-12-19"
lastmod: "2024-02-11"
categories: []
tags: [Mitgliedschaften, Transparenz]
weight: 2
draft: false
hidden: true
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
---

## Mitgliedschaften

| Name der Organisation                                                                                             | Funktion                    | Zeitraum            | Anmerkung(en) |
| :---------------------------------------------------------------------------------------------------------------- | :-------------------------- | :------------------ | :------------ |
| [Jugendpresse Rheinland e. V. (JPR)](https://jugendpresse.nrw)                                                    | Mitglied                    | 01/2017 - heute     |               |
| [Codeberg e. V.](https://codeberg.org)                                                                            | Fördermitgliedschaft        | 11/2021 - heute     |               |
| [Allgemeiner Deutscher Automobilclub (ADAC)](https://adac.de)                                                      | Mitgliedschaft              | ~09/2022 - heute    | Fahrschule    |
| [DRK-Kreisverband Gelsenkirchen e. V. (DRK GE)](https://drk-ge.de)                                                | Mitgliedschaft              | 09/2022 - heute     |               |
| [Allgemeiner Deutscher Fahrradclub (ADFC)](https://adfc.de)                                                       | Mitgliedschaft              | 06/2023 - heute     |               |
| [Gesellschaft für Freiheitsrechte e. V. (GFF)](https://freiheitsrechte.org)                                       | Fördermitgliedschaft        | 07/2023 - heute     |               |
| [Netzbegrünung e. V.](https://netzbegruenung.de)                                                                  | Mitgliedschaft              | 11/2023 - heute     |               |
| [Verein für Leibesübungen Bochum 1848 Fußballgemeinschaft e. V. (VfL Bochum 1848) ](https://www.vfl-bochum.de/de) | Mitgliedschaft              | 12/2023 - heute     |               |

## Vereinsfunktionen

| Name der Organisation                                                                                             | Funktion                    | Zeitraum            | Anmerkung(en) |
| :---------------------------------------------------------------------------------------------------------------- | :-------------------------- | :------------------ | :------------ |
| [Jugendpresse Rheinland e. V. (JPR)](https://jugendpresse.nrw)                                                    | Geschäftsführender Vorstand | 02/2019 bis 02/2021 |               |
| [Jugendpresse Rheinland e. V. (JPR)](https://jugendpresse.nrw)                                                    | Geschäftsführender Vorstand | 04/2023 bis heute   |               |
