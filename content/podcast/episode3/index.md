---
title: "Folge 3: Wie steht es um die IT-Sicherheit in Deutschland und Europa "
date: "2025-02-12"
lastmod: "2025-02-22"
author: "Lukas Schieren"
description: " Dieses Mal wird der Podcast „/var/log/podcast“ sich mit dem Thema IT-Sicherheit auseinandersetzen."
categories: [Podcast]
tags: [Podcast, VarLogPodcast, LukasPodcast, 38c3, IT-Sicherheit]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
eptype: full
number: 3
duration: "01:01:24"
mp3:
  asset_link: "/podcast/episode3/index.mp3"
  length: 43107624
disableToC: false
disableAutoCollapse: true
type: podcast
layout: episode
comments:
  host: social.lukas-schieren.de
  username: podcast
  id: 114048341404729904
image:
  url: "/podcast/podcast-cover.png"
  alt: "Cover des /var/log/podcast Podcast>"
---

Dieses Mal wird der Podcast „/var/log/podcast“ sich mit dem Thema IT-Sicherheit auseinandersetzen.

<!--more-->

IT-Sicherheit? Was genau ist das und wie sieht es in Deutschland und Europa aus? Was gibt es in Bezug auf die IT-Sicherheit kritischer Infrastruktur (KRITIS)?

Danach betrachten wir das Jahr 2025 und die Problemstellungen, Neuerungen und sonstigem im Bereich der IT-Sicherheit.

## Shownotes

- [Checkliste Phishing](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Checklisten/BSI-ProPK-Checkliste-Phishing.pdf?__blob=publicationFile&v=1)
- [Checkliste Onlinebanking](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Checklisten/BSI-ProPK-Checkliste-Onlinebanking.pdf?__blob=publicationFile&v=)
- [Checkliste Schadsoftware](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Checklisten/BSI-ProPK-Checkliste-Schadsoftware.pdf?__blob=publicationFile&v=1)
- [mobilsicher.de Schadprogramme auf Smartphone](https://mobilsicher.de/ratgeber/schadprogramme-auf-dem-smartphone)
- [NIS-2 Richtlinie](https://de.wikipedia.org/wiki/NIS-2-Richtlinie)
- [Referentenentwurf aus dem Bundesministerium für Inneres und Heimat (BMI) von Nancy Faeser (SPD) nach Werkstattgespräch](https://www.bmi.bund.de/SharedDocs/gesetzgebungsverfahren/DE/Downloads/referentenentwuerfe/CI1/NIS-2-RefE.pdf?__blob=publicationFile&v=7)
- [Einbringung eines Gesetzesentwurfes durch die BuReg auf BT-Drucksache 20/13184](https://dserver.bundestag.de/btd/20/131/2013184.pdf)
- [Anhörung zum Gesetzesentwurf in der 90. Sitzung des Ausschusses für Inneres und Heimat am 4. November 2024](https://www.bundestag.de/resource/blob/1038526/8c8839889e7d24c95f7a2cfb0e0cf440/Protokoll-04-11-2024-11-00-Uhr.pdf)
- [Bundesamt für Sicherheit in der Informationstechnik (BSI) auf Ausschussdrucksacke 20(4)523 C](https://www.bundestag.de/resource/blob/1027138/0e7d977acf676897233c034b842807be/20-4-523-C.pdf)
- [Bundesbeauftragte für den Datenschutz und die Informationsfreiheit (BfDI) auf Ausschussdrucksacke 20(4)529](https://www.bundestag.de/resource/blob/1027552/8ccf03812e698b9473fa834cb4192213/20-4-529.pdf)
- [Arbeitsgruppe Kritische Infrastrukturen (AG KRITIS) auf Ausschussdrucksacke 20(4)528](https://www.bundestag.de/resource/blob/1027334/3e421efc0ba6e141725e9f9f18a5b318/20-4-528.pdf)

## Timestamps

00:00:00 Begrüßung & Einleitung

00:04:55 IT-Sicherheit Grundlagen

00:30:30 Umgang mit Schadsoftware

00:34:47 IT-Sicherheit und kritische Infrastruktur Teil I

00:38:57 Einschub: Wählen

00:39:12 IT-Sicherheit und kritische Infrastruktur Teil II

00:54:34 Outro
