---
title: "Folge 1: Chaos Communication Camp 2023"
date: "2023-10-16"
author: "Lukas Schieren"
lastmod: "2023-10-16"
author: Lukas Schieren
description: "Hier ist meine erste Podcast-Folge mit dem Thema Chaos Communication Camp gemeinsam mit Ückück. Viel Spaß!"
categories: [Podcast]
tags: [Podcast, VarLogPodcast, LukasPodcast, CCCamp23 CCCamp2023]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
eptype: full
number: 1
duration: "00:51:48"
mp3:
  asset_link: "/podcast/episode1/index.mp3"
  length: 56073992
disableToC: false
disableAutoCollapse: true
type: podcast
layout: episode
comments:
  host: social.lukas-schieren.de
  username: podcast
  id: 111208393179617225
image:
  url: "/podcast/podcast-cover.png"
  alt: "Cover des /var/log/podcast Podcast>"
---

Viel Spaß beim hören meiner ersten Podcast-Folge zum Chaos Communication Camp 2023 (CCCamp23) gemeinsam mit Ückück. 

<!--more-->

Ückück (@ueckueck@dresden.network), die einzigwahre Fediverse-Erklärtante, Teil der Moderation auf der Mastodon-Instanz dresden.network, Piratin (2. Vorsitzende der Piraten Sachsen & Politische Geschäftsführung der Piraten Dresden. Ansonsten noch Teil vom @AKND3 :aknd3, vom @c3d2 & der @chaoszone.


