---
title: "Folge 2: Wie kann eine Verwaltungsdigitalisierung in Deutschland und Europa nachhaltig und bevölkerungsfreundlich gestaltet werden?"
date: "2024-01-06"
lastmod: "2024-01-06"
author: "Lukas Schieren gemeinsam mit Marco Bakera und drei weiteren Menschen vom 37c3"
description: " In dieser Episode geht es um Verwaltungsdigitalisierung - live auf dem 37c3 aufgenommen."
categories: [Podcast]
tags:
  [
    Podcast,
    VarLogPodcast,
    LukasPodcast,
    37c3,
    Verwaltungsdigitalisierung,
    FediLZ,
    EduDE,
    Digitalisierung der Verwaltung,
  ]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
eptype: full
number: 2
duration: "00:55:32"
mp3:
  asset_link: "/podcast/episode2/index.mp3"
  length: 42370531
disableToC: false
disableAutoCollapse: true
type: podcast
layout: episode
comments:
  host: social.lukas-schieren.de
  username: podcast
  id: 111664064700889743
image:
  url: "/podcast/podcast-cover.png"
  alt: "Cover des /var/log/podcast Podcast>"
---

## Shownotes

- [Heise Artikel zu SSI und EuID](https://www.heise.de/news/Hunderte-Wissenschaftler-warnen-vor-staatlichen-Root-Zertifikaten-9355165.html)

- [Dashboard Digitale Verwaltung (DDV)](https://dashboard.ozg-umsetzung.de/)

- [Studiengang "Verwaltungsdigitalisierung"](https://vwa-freiburg.de/studiengang/verwaltungsdigitalisierung/)

- [Ohne Open Source? Wie hätte das denn gehen sollen? Geschichten aus der Digitalisierung von Verwaltungen inmitten der Pandemie" von Bianca Kastl](https://bkastl.de/froscon23/)

- [Einer für alle«-Prinzip erweist sich als Fehlschlag (15.12.2023)](https://www.spiegel.de/politik/deutschland/online-buergerservice-einer-fuer-alle-prinzip-erweist-sich-als-fehlschlag-a-cb093dfd-ef1b-4916-837f-c1f1389bde07)

- ["Reicht nicht nur, Anträge online anzubieten" Deutscher Städtetag](https://www.staedtetag.de/presse/pressemeldungen/2023/kommunalpakt-verwaltungsdigitalisierung-reicht-nicht-nur-antraege-online-anzubieten)

- [Kleine Anfrage von der Abgeordneten Anke Domscheit-Berg sowie weiteren MdBs der Fraktion die Linke mit dem Titel "Status quo und Fortschritt bei der Nachhaltigkeit der Informationstechnologie des Bundes" unter Drucksache 20/3164](https://dserver.bundestag.de/btd/20/031/2003164.pdf) (23.08.2023)

- [Beschluss: Green-IT-Initiative des Bundes; Beschluss Nr. 2022/05 aus 05/2022](https://www.cio.bund.de/SharedDocs/downloads/Webs/CIO/DE/it-rat/beschluesse/beschluss_05_2022_green_it.pdf?__blob=publicationFile&v=3)

- ["Blauer Engel: Von 177 Rechenzentren des Bundes ist nur eines besonders umweltfreundlich"](https://www.spiegel.de/netzwelt/netzpolitik/blauer-engel-von-177-rechenzentren-des-bundes-ist-nur-eins-besonders-umweltfreundlich-a-99ef342d-9744-4ef3-9f08-24d4f059b151) (29.12.2020) ; nach alten Richtlinien; nach neuen kein einiziges

## Timestamps

00:00:00 Begrüßung & Einleitung

00:00:47 Wie steht es aktuell um die Verwaltungsdigitalisierung in Deutschland, im besonderen unter dem Aspekt Nachhaltigkeit

00:24:35 Einordnung zum Redebeitrag von Ralf Knoblauch zum Thema Selbstbestimmte Identität (SSI)

00:25:11 Wie steht es aktuell um die Verwaltungsdigitalisierung in Deutschland, im besonderen unter dem Aspekt Nachhaltigkeit

00:38:35 Inwiefern kann der vollständige (sukzessive) Umstieg bzw. insgesamt der Einsatz freier Softwarelösungen in (deutschen) Behörden und der öffentlichen Verwaltung dabei helfen, die Digitalisierung dort nachhaltiger zu gestalten?

00:43:26 Wie steht es um die Interoperabilität?

00:49:08 Wie steht es um Barrierefreiheit, Teilhabe für Alle, Inklusion bei den digitalen Verwaltungsdienstleistungen am Beispiel einer Schulverwaltungssoftware aus NRW?

00:53:42 Fazit

00:54:33 Outro
