---
title: "Folge 2: Wie kann eine Verwaltungsdigitalisierung in Deutschland und Europa nachhaltig und bevölkerungsfreundlich gestaltet werden?"
date: "2023-12-28"
author: "Lukas Schieren"
lastmod: "2023-12-28"
author: Lukas Schieren
description: " Viel Spaß!"
categories: [Podcast]
tags: [Podcast, VarLogPodcast, LukasPodcast, 37c3, Verwaltungsdigitalisierung, FediLZ, EduDE, Digitalisierung der Verwaltung]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
eptype: full
number: 2
duration: "00:00:00"
mp3:
  asset_link: "/podcast/episode2/index.mp3"
  length: 56073992
disableToC: false
disableAutoCollapse: true
layout: episode
comments:
  host: social.lukas-schieren.de
  username: podcast
  id: 111664064700889743
image:
  url: "/podcast/podcast-cover.png"
  alt: "Cover des /var/log/podcast Podcast>"
---
