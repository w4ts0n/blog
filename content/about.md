---
title: "Über den Blog"
subtitle: ""
author: "Lukas Schieren"
date: "2022-07-14"
lastmod: "2022-07-14"
categories: []
tags: []
weight: 2
draft: false
enableDisqus: false
enableMathJax: false
toc: true
hidden: true
disableToC: false
disableAutoCollapse: true
aliases:
  - /public/about
---

# Allgemeines

Bei diesem digitalen Blog handelt es sich primär um ein privates und nicht profitorientiertes Projekt von Lukas Schieren. Sekundär werden Blogbeiträge als Nachweise für eine journalistische Tätigkeit zum Erwerb eines Jugendpresse-Ausweises (JPA) der Jugendpresse Deutschland, dem Bundesverband der Jugendpressen in Deutschland, eingereicht – diese werden als solche gekennzeichnet und in einer gesonderten – chronologisch geordneten – Liste aufgeführt.

Spenden nimmt der Blog aktuell nicht an.

# Leitprinzipien des Blogs

- Es wird in allen Komponenten des Blogs auf jedwede Einbindung von Werbeinhalten aller Art verzichtet.

- Es wird in allen Komponenten des Blogs auf jedwede stigmatisierende Sprache (in Form von Begriffen, Sätzen, Phrasen und oder Stereotypen) verzichtet.

- Es werden in allen Komponenten des Blogs, bei Themen, die dies erfordern (bspw. mentale oder physische Gesundheit), Inhaltswarnungen vorangestellt.

- Es werden in allen eingebundenen Bildern, Videos oder Audiodateien innerhalb von Blogbeiträgen aus Rücksichtnahme auf Menschen mit Hör- und/oder Sehbeeinträchtigungen ein Alternativtext eingefügt.

- Der Seitenquelltexts dieses Blogs inklusive aller Änderungen an diesem, sind aus Gründen der Transparenz vollständig einsehbar unter: https://codeberg.org/w4ts0n/blog

- Es werden die publizistischen Grundsätze inbegriffen der sich daraus ableitenden Pflichten des deutschen Presserats (auch Pressekodex genannt) befolgt.

---

Hinweis: Der Pressekodex des deutschen Presserats in der aktuell gültigen Fassung vom 11. September 2019 ist in deutscher und englischer Sprache einsehbar unter https://www.presserat.de/downloads.html?file=files/presserat/dokumente/pressekodex/Pressekodex2021.pdf (deutsch) und https://www.presserat.de/downloads.html?file=files/presserat/dokumente/download/Press%20Code.pdf (english)

---

# Über welche Themen werde ich bloggen?

- IT
  - Open Souce Soft- und Hardware-Projekte
  - Datenschutz
- Ehrenamt
- Gedanken zu landes-, bundes- und/oder EU-politischen Sachverhalten

# Ab wann wird ein Blogbeitrag ins Archiv verschoben?

Blogbeiträge, welche Teil einer Artikelserie sind, werden i.d.R. erst nach vollständigen Abschluss der Artikelserie oder aber spätestens fünf Jahre nach dem Veröffentlichungsdatum ins Archiv verschoben.

Blogbeiträge, welche **kein** Teil einer Artikelserie sind, werden zweieinhalb Jahre nach dem Veröffentlichungsdatum ins Archiv verschoben.
