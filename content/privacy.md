---
title: "Datenschutzhinweise"
date: "2022-07-14"
lastmod: "2023-08-12"
author: "Lukas Schieren"
hidden: true
aliases:
  - /public/privacy
---

# Informationspflicht bei Erhebung von personenbezogenen Daten gemäß Art. 13 DS­-GVO

## Angaben zum Verantwortlichen

Lukas Schieren

Gebrüder-Coblenz-Str. 12

50679 Köln

| E-Mail-Adresse                                          | OpenPGP-Schlüssel                                           | Fingerabdruck                                     |
| :------------------------------------------------------ | :---------------------------------------------------------- | :------------------------------------------------ |
| [info@lukas-schieren.de](mailto:info@lukas-schieren.de) | [Schlüsseldatei](../keys/pubkey_info@lukas-schieren.de.asc) | A8EE E280 4C78 10CF 218F 46EA 3576 61F9 4488 05A2 |

## Mit dem Datenschutz beauftragte Person

Bei Fragen zum Datenschutz steht Ihnen unsere mit dem Datenschutz beauftragte Person unter [datenschutz@lukas-schieren.de](mailto:datenschutz@lukas-schieren.de) oder unter der oben angegebenen postalischen Anschrift mit dem Zusatz "Mit dem Datenschutz beauftragte Person" zur Verfügung.

| E-Mail-Adresse                                                        | OpenPGP-Schlüssel                                                  | Fingerabdruck                                     |
| :-------------------------------------------------------------------- | :----------------------------------------------------------------- | :------------------------------------------------ |
| [datenschutz@lukas-schieren.de](mailto:datenschutz@lukas-schieren.de) | [Schlüsseldatei](../keys/pubkey_datenschutz@lukas-schieren.de.asc) | D9D1 3415 1683 77B4 8A36 DC8B 8BBC DBCD 4C07 194A |

## Verarbeitung Ihrer personenbezogenen Daten – Zwecke und Rechtsgrundlagen

### Datenverarbeitung auf der Webseite

Hinweis: Diese Webseite wird über Codeberg Pages (https://codeberg.page) bereitgestellt. Weitere Informationen neben den hier genannten, sind unter folgendem Link zu entnehmen: https://codeberg.org/Codeberg/org/src/branch/main/de/Datenschutz.md

Die Kommentarfunktion des Blogs und Podcast wird über Fediverse-Beiträge für jeden einzelnen Blog-Post und Podcast-Episode auf einer eigenen - bei [Weingärtner IT Services](https://weingaertner-it.de/) gemieteten Mastodon-Instanz (https://social.lukas-schieren.de) realisiert.

#### Logfiles

Bei Aufruf unserer Webseite werden Logfiles gesetzt und bleiben für sieben (7) Tage gespeichert.

### Datenverarbeitung im Unternehmen

Wir verarbeiten personenbezogene Daten auf Grundlage von datenschutzrechtlichen Regelungen der Datenschutz-Grundverordnung (DS-GVO) sowie des Bundesdatenschutzgesetzgesetzes (BDSG) und ggf. den Landesdatenschutzgesetzen der einzelnen Bundesländer. Die Daten der folgenden Personengruppen werden von den jeweils zuständigen Personen im Unternehmen zur Aufgabenerfüllung verarbeitet.

## Speicherdauer

Die von uns gespeicherten personenbezogenen Daten werden nach Maßgabe von gesetzlichen Vorgaben gelöscht. Wir löschen die Daten, sobald sie für den Verarbeitungszweck nicht mehr erforderlich sind, eine gegebene Einwilligung widerrufen wird oder sonstige Erlaubnisse entfallen.
Daten, die z.&nbsp;B. aus handels- oder steuerrechtlichen Gründen noch aufbewahrt werden müssen oder deren Speicherung noch für die Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich ist, werden gelöscht, sobald dies nicht mehr der Fall ist.

## Betroffenenrechte

Wenn wir personenbezogene Daten von Ihnen verarbeiten, haben Sie folgende Betroffenenrechte:

- ein Recht auf Auskunft über die verarbeiteten Daten und auf Kopie,
- ein Berichtigungsrecht, wenn wir falsche Daten über Sie verarbeiten,
- ein Recht auf Löschung, es sei denn, dass noch Ausnahmen greifen, warum wir die Daten noch speichern, also zum Beispiel Aufbewahrungspflichten oder Verjährungsfristen
- ein Recht auf Einschränkung der Verarbeitung,
- ein jederzeitiges Recht, Einwilligungen in die Datenverarbeitung zu widerrufen,
- ein Widerspruchsrecht gegen eine Verarbeitung im öffentlichen oder bei berechtigtem Interesse,
- ein Recht auf Datenübertragbarkeit,
- ein Beschwerderecht bei einer Datenschutz-Aufsichtsbehörde, wenn Sie finden, dass wir Ihre Daten nicht ordnungsgemäß verarbeiten. Für unser Unternehmen ist die Landesbeauftragte für Datenschutz und Informationsfreiheit Nordrhein-Westfalen zuständig. Wenn Sie sich in einem anderen Bundesland oder nicht in Deutschland aufhalten, können Sie sich aber auch an die dortige Datenschutzbehörde wenden.

Diese Datenschutzerklärung wurde mit dem "dsgvo.clever"-Werkzeug des Landesbeauftragten für den Datenschutz und Informationsfreiheit Baden-Württemberg (LfDI BaWü) erstellt.
