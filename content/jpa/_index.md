---
title: "Nachweise der journalistischen Tätigkeit zum Erwerb des Jugendpresse-Ausweises (JPA)"
author: "Lukas Schieren"
date: "2022-07-24"
lastmod: "2023-01-27"
hidden: true
categories: []
tags: [Transparenz, Transparency, Leitbild]
weight: 2
draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
---

# Allgemeines

Die Liste an Nachweisen der journalistischen Tätigkeit zum Erwerb des Jugendpresse-Ausweises (JPA) ist unter folgenden [Verweis]({{< ref "/tags/jpa" >}}) zu finden.

Neben der Kenntlichmachung von Nachweisen der journalistischen Tätigkeit zum Erwerb des Jugendpresse-Ausweises (JPA) im Blog selber (Liste auf [Transparenzseite]({{< ref "transparency.md" >}}), Hinweis in jedem betreffenden Blogbeitrag und Zuweisung von entsprechendem Schlüsselwort / Tag), gibt es eine Kennzeichnung mithilfe von Labeln im Codeberg-Repository, wo der gesamte Quellcode dieses Blogs öffentlich einsehbar ist.

Es wird unterschieden zwischen vorgemerkten, eingereichten und akzeptieren Beiträgen.

Stufe I Vorgemerkt: Ein Blogbeitrag soll nach der Veröffentlichung als Nachweise der journalistischen Tätigkeit zum Erwerb des Jugendpresse-Ausweises (JPA) eingereicht werden. Eine Liste jedweder Beiträge, welche sich in diesem Status befinden, ist unter folgendem Link zu finden:
https://codeberg.org/w4ts0n/blog/issues?labels=67511

Stufe II: Eingereicht: Der Blogbeitrag wurde beim zuständigen Landesverband eingereicht. Eine Liste jedweder Beiträge, welche sich in diesem Status befinden, ist unter folgendem Link zu finden: https://codeberg.org/w4ts0n/blog/issues?labels=67510

Stufe III: Akzeptiert: Der zuständige Landesverband hat die eingereichten Nachweise journalistischer Tätigkeit nach den Erwerbskriterien für den JPA geprüft und für hinreichend befunden. Eine Liste jedweder Beiträge, welche sich in diesem Status befinden, ist unter folgendem Link zu finden: https://codeberg.org/w4ts0n/blog/issues?labels=67511

# Wer ist berechtigt einen Jugendpresse-Ausweis (JPA) zu beantragen?

Grundsätzlich sind alle Mitglieder von Jugendpresseverbänden, die Mitglied in der Jugendpresse Deutschland e. V. sind und zusätzlich das 27. Lebensjahr noch nicht vollendet haben, antragsberechtigt. Näheres hierzu regelt Paragraph 2 Absatz 1 die Buchstaben a bis e der bundeseinheitlichen Jugend-Presseausweis-Ordnung(vgl. https://jugendpresse.nrw/wp-content/uploads/2020/04/PDF/JPA_web.pdf).

# Wer stellt den JPA aus?

Den JPA stellt der jeweilge Landesverband der Jugendpresse Deutschland e. V aus. 

Eine Liste der Landesverbände ist auf der Seite der Jugendpresse Deutschland zu finden: https://jugendpresse.de/landesverb%C3%A4nde

Die jeweils zuständigen Landesverbände prüfen die Vorrausetzungen und stellen - bei Erfüllung der Vorrausetzungen - den JPA aus (vgl. § 1 Nr. 1 S. 1 Jugend-Presseausweis-Ordnung).

Für Nordrhein-Westfalen (NRW) ist die Jugendpresse Rheinland e. V. mit Sitz in rheinländischen Köln zuständig.

# Welche Anforderungen gibt es an die Nachweise der journalistischen Tätigkeit zum Erwerb des JPA

- Die Nachweise entsprechen den journalistischen Ansprüche gemäß des Pressekodexs
- Die Veröffentlichungsdaten der Nachweise liegt bei bei Einsendung nicht mehr als sechs Monate zurück.

# Wie lange ist der Jugend-Presseausweis gültig?

Die Gültigkeit des JPA endet mit Ende eines Kalenderjahres (31. Dezember). Der bundeseinheitlichen Jugend-Presseausweis-Ordnung gemäß, können Nachweise für eine Verlängerung des JPA bis spätestens zum 31. Januar des Folgejahres beim zuständigen Landesverband der Jugendpresse Deutschland eingereicht werden.

# Weitere Informationen

- zur Jugendpresse Rheinland (JPR) unter https://jugendpresse.nrw/jugend-presseausweis/
- zur Jugendpresse Deutschland (JPD) unter https://jugendpresse.de/
- zum Jugend-Presseausweis (JPA) unter https://jugend-presseausweis.de/#faq
