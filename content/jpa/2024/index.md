---
title: "Beiträge aus 2024 für den Jugendpresse-Ausweis (JPA) in 2025"
author: "Lukas Schieren"
date: "2024-10-02"
lastmod: "2024-11-16"
categories: [Transparenz, JPA, JugendpresseAusweis]
tags: [Transparenz, JPA, JugendpresseAusweis]
weight: 2
draft: false
hidden: true
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
---

| Titel des Beitrages                                                                           | Veröffentlicht am | Zuletzt aktualisiert am | Status                           | Anmerkung(en) |
| :-------------------------------------------------------------------------------------------- | :---------------- | :---------------------- | :------------------------------- | :------------ |
| [Fediverse-Profil des Jugendforum NRW](https://nrw.social/@JugendforumNRW)                    | 21. August 2024   | 25. August 2024         | Eingereicht am 16. November 2024 |               |
| [Fediverse-Profil der Jugendpresse Rheinland](https://nrw.social/@JugendpresseRheinland)      | 1. Januar 2024    | 7. September 2024       | Eingereicht am 16. November 2024 |               |
| [Messebericht: Florian Messe 2024 in Dresden]({{< ref "post/2024/florian-messe/index.md" >}}) | 16. November 2024 | 16. November 2024       | Eingereicht am 16. November 2024 |               |
