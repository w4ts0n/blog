---
title: "Beiträge aus 2023 für den Jugendpresse-Ausweis (JPA) in 2024"
author: "Lukas Schieren"
date: "2023-12-19"
lastmod: "2024-10-02"
categories: [Transparenz, JPA, JugendpresseAusweis]
tags: [Transparenz, JPA, JugendpresseAusweis]
weight: 2
draft: false
hidden: true
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
---

| Titel des Beitrages                                                              | Veröffentlicht am  | Zuletzt aktualisiert am | Status                           | Anmerkung(en)                    |
| :------------------------------------------------------------------------------- | :----------------- | :---------------------- | :------------------------------- | :------------------------------- | --- |
| Fediverse-Profil des Jugendforum NRW                                             | 23. August 2023    | 27. August 2023         | Eingereicht am 13. Dezember 2023 |                                  |
| Lauer Musikabend mit dem Rock Orchester Ruhrgebeat im Amphitheater Gelsenkirchen | 14. September 2023 | 14. September 2023      | Eingereicht am 13. Dezember 2023 |                                  |
| Episode 1: Chaos Communication Camp 2023                                         |                    | 16. Oktober 2023        | 16. Oktober 2023                 | Eingereicht am 13. Dezember 2023 |     |
| Night of the Proms in Köln                                                       | 5. Dezember 2023   | 5. Dezember 2023        | Eingereicht am 13. Dezember 2023 |                                  |
| Sarah Connor swingt auf ihrer Tour in die Weihnachtszeit                         | 10. Dezember 2023  | 10. Dezember 2023       | Eingereicht am 13. Dezember 2023 |                                  |
