---
title: "Beiträge aus 2022 und 2023 für den Jugendpresse-Ausweis (JPA) in 2023"
author: "Lukas Schieren"
date: "2023-12-19"
lastmod: "2024-10-02"
categories: [Transparenz, JPA, JugendpresseAusweis]
tags: [Transparenz, JPA, JugendpresseAusweis]
weight: 2
draft: false
hidden: true
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
---

| Titel des Beitrages                                                                                                                                                                                                                  | Veröffentlicht    | Zuletzt aktualisiert | Status     | Anmerkung(en)                                                                                                    |
| :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :---------------- | :------------------- | :--------- | :--------------------------------------------------------------------------------------------------------------- |
| [Fediverse - können soziale Netzwerke losgelöst von kommerziellen Interessen und im Einklang mit europäischen Gesetz funktionieren?]({{< ref "post/2022/fediverse.md" >}})                                                           | 24. August 2022   | 29. August 2022      | Akzeptiert | Eingereicht am 21. Januar 2023. Akzeptiert am 27. Januar 2023                                                    |
| [In eigener Sache: Wie entsteht ein Blogbeitrag & wie funktioniert der Blog technisch?]({{< ref "post/2023/how-to-blog/index.md" >}})                                                                                                | 20. Januar 2023   | 29. Januar 2023      | Akzeptiert | Eingereicht am 21. Januar 2023. Akzeptiert am 27. Januar 2023                                                    |
| [Rock Orchester Ruhrgebeat seit 10 Jahren mit Glockenrock unterwegs - In Gelsenkirchen Rotthausen wurde Glockenrock geboren](https://www.eventmagazin-online.de/rock-orchester-ruhrgebeat-seit-10-jahren-mit-glockenrock-unterwegs/) | 07. November 2022 | 07. November 2022    | Akzeptiert | Dieser Beitrag wurde nicht im Blog Veröffentlicht. Eingereicht am 21. Januar 2023. Akzeptiert am 27. Januar 2023 |
