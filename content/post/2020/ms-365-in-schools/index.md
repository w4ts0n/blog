---
title: "Gehört Microsoft (Office) 365 aus Bildungseinrichtungen verbannt?"
date: "2020-12-31"
author: Lukas Schieren
description: "Eine beachtliche Anzahl an Bildungseinrichtungen haben nach dem ersten Shutdown Anfang des Jahres sich für Microsoft (Office) 365 und langfristig gesehen auch für außerordentlich viele Abhängigkeiten entschieden. Wenn man sich aber als Bildungseinrichtungen von einer Abhängigkeit in die nächste stolpert, kann das in meinen Augen nicht zielführend sein."
categories: [Kommentar]
tags:
  [ms365, Microsoft, Microsoft 365, Office365, FediLZ, Bildungseinrichtungen]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
aliases:
  - /public/post/2020/ms-365-in-schools/
  - /commentary/ms-365-in-schools/
  - /2020/12/ms-365-in-schools/
comments:
  host: social.lukas-schieren.de
  username: blog
  id:
---

Die Corona Pandemie zeigte Anfang des Jahres, wie ich finde auf eine sehr eindrucksvolle Art und Weise, wie es um die Digitalisierung in deutschen Bildungseinrichtungen steht – teilweise sehr schlecht. Der Extra-3 Satiriker Christian Ehring fasste es in meinen Augen super zusammen: Er sagte, dass wenn Corona ein Computervirus sei, deutsche Schulen fein raus wären.

Noch immer prägen zumeist Overheadprojektoren und Kreidetafeln, anstelle Beamer und intelligente Tafeln, das Bild in deutschen Klassenräumen. Außerdem brachte die Pandemie viele Bildungseinrichtungen in Zugzwang so schnell, wie es nur irgendwie möglich war, eine zentrale digitale Plattform für die gemeinsame Zusammenarbeit von Lehrer:innen und Schüler:innen, damit man für den Distanzunterricht gewappnet war, bereitzustellen. Und von da an setzten viele Einrichtungen, in Ermangelung an Erfahrung und Alternativen, auf die von Microsoft bereitgestellte Lösung Microsoft (Office) 365.

## Was ist schlecht am Einsatz von Microsoft 365 in Bildungseinrichtungen?

Mit dem Einsatz von Microsoft 365 in Bildungseinrichtungen kommt auf lange Zeit gesehen in diesen zu absoluten Abhängigkeiten und es ist gar unmöglich auf Systeme, wie Nextcloud umsteigen zu können, weil man auf die Microsoft Produkte zu stark angewiesen ist und im Kreislauf der Abhängigkeiten gefangen ist und es sehr viel Aufwand ist, daraus ausbrechen zu können. Es schadet außerdem auch der digitalen Souveränität der Bildungseinrichtungen, welche eigentlich doch so sollte man meinen souverän bleiben sollten – auch digital.

Der amtierende Bundesbeauftragte für Datenschutz und die Informationsfreiheit Prof. Ulrich Kelber traf auf dem Anfang November stattgefundenen deutschen Betriebsräte Tag, eine Aussage, die man so auch für den Bildungssektor überleiten kann – er ist der Auffassung, dass Datenschutz ein Grundrecht sei und somit auch in Krisensituationen [wie aktuell durch den Coronavirus SARS-CoV-2] dieses nicht vernachlässigt werden darf.

Warum ist was schlecht am Einsatz von Microsoft 365 in Bildungseinrichtungen?
In Bildungseinrichtungen wird mit personenbezogenen Daten von jungen Menschen, zumeist Kindern und Jugendliche gearbeitet, welche besonderen Schutz bedürfen. Der Rechtsanwalt Oliver Rosbach hat in einem Gastbeitrag von Netzpolitik.org exemplarisch an einer bayrischen Schule die Datenschutzverstöße durch eine zwanghafte Nutzung von Microsoft 365 der Schule untersucht und kommt schlussendlich zum Schluss, dass ein Einsatz von Microsoft 365 mit groben Verletzungen datenschutzrechtlicher Vorschriften einher geht, beispielsweise gegen das Gebot der Privacy by default, welche in Artikel 25 der DS-GVO geregelt ist, welches mit der Voreinstellung der Telemetriedatenübertragung nicht gewährleistet wird. Das Bundesamt für Sicherheit in der Informationstechnik stellte außerdem 2018 fest, dass sich diese auch nicht vollständig abstellen lassen. Außerdem ist in diesem Kontext auch interessant zu erwähnen, dass durch die wiederholte Kippung des ePrivacy Shield Abkommen Mitte des Jahres, und damit die rechtliche Grundlage von Produkten, wie ZOOM oder Microsoft 365 fehlt und somit innerhalb der europäischen Union nicht gemäß der Datenschutz-Grundverordnung eingesetzt werden darf.

Dazu kommt, dass es in den U.S.A. den CLOUD Act gibt, welcher den Strafverfolgungsbehörden der USA die Möglichkeit (ohne richterliche Beschlüsse) einräumt auf Serverdaten von US-amerikanischen Firmen und somit auch auf Daten von US-Firmen in Europa – das ist vor dem Hintergrund der besonders schützenswerten Daten von Jungen Menschen mehr als bedenklich.

## Eigener Standpunkt zur Frage, ob Microsoft 365 aus Bildungseinrichtungen verbannt gehört:

Um es kurz zu machen: Ja! Denn ich bin der Meinung, dass Nextcloud durch die Möglichkeit des selber Betreibens und damit die Kontrolle seiner Daten einfach unschlagbar ist. Das einzige was Nextcloud für Bildungseinrichtungen noch fehlt, was aktuell Microsoft 365 auch nicht abbilden kann: Ein Vertretungsplan und ein digitales Klassenbuch – aber: Ich bin zuversichtlich, aufgrund der großen aktiven Community von Nextcloud, dass diese Funktionalitäten nicht lange auf sich warten lassen werden.

## Wie könnte eine moderate Alternative auf Basis freier und quelloffener Softwarelösungen aussehen?

Die Medienwerkstatt Minden-Lübbecke hat in einem Video auf Ihrem YouTube-Kanal exemplarisch - wie ich finde sehr gut - aufgezeigt, wie man als Bildungseinrichtung eine Nextcloud effizient und effektiv als Produktivumgebung einsetzen kann. Die Medienwerkstatt würde die Erweiterung „Kreise“, welche eine Verfeinerung der Gruppen darstellt, einsetzen wollen, um Schüler:innen Klassen und Kursen zuzuweisen.

Videokonferenzen mit mehr als drei bis vier Personen über den Nextcloud eigenen Audio- und Videokonferenzdienst Nextcloud Talk, lassen sich nach Einschätzung der Medienwerkstatt leider nicht bewerkstelligen und man müsste für den Distanzunterricht auf die freie und quelloffene Softwarelösung BigBlueButton, welche die Alternative zu ZOOM darstellt, ausweichen, welche sich aber dank bereits vorhandener Integration wunderbar in Nextcloud integrieren lässt.

Bei der Frage der Administration wäre es möglich auf Hostinganbieter, wie Nextcloud Partner IONOS oder Hetzner Online zu setzen, welche entsprechend versichert sind und das nötige Personal, um die Sicherheit dauerhaft gewährleisten zu können. Nextcloud Gründer Frank Karlitschek sagte im Nextcloud Blog, dass beispielsweise mit dem Nextcloud Partner IONOS innerhalb einer Woche eine siebenstellige Anzahl an Schüler:innen ohne Probleme mit Nextcloud versorgt werden könne.

## Interesting to know

Auch Das Informationszentrum Bund (ITZBund) hatte im April 2018 bekannt gegeben, dass Sie für Bundesministerien und andere Einrichtungen des Bundes auf Nextcloud setzen wollen. Auch die EU-Staaten Frankreich, Schweden und die Niederlande haben im August 2019 angekündigt, zukünftig auf Nextcloud setzen zu wollen.
