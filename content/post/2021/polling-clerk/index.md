---
title: Wahlhelfer:innen-Ehrenamt - eine Möglichkeit für junge Menschen an unserer Demokratie zu partizipieren?
date: "2021-11-02"
lastmod: "2023-06-24"
author: Lukas Schieren

description: "Alle paar Jahre wieder stehen bei uns entweder in der Kommune, im Bundesland, im Bund oder in Europa Wahlen nach demokratischen Grundsätzen an. Damit bei diesen Wahlen die demokratischen Wahlgrundsätze eingehalten werden können, ist die Demokratie auf ehrenamtliches Engagement der wählenden Bevölkerung angewiesen. Kurz um: Wahlhelfer:innen sind für das ordnungsgemäße Durchführen von Wahlen in einer Demokratie absolut systemrelevant! Dies ist ein Erfahrungsbericht nach zwei Einsätzen als Wahlhelfer (Kommunal- und Bundestagswahl) - beide Male als Erstwähler."
categories: [Erfahrungsbericht]
tags: [wahlen, election, btw21, bundestagswahl21, demokratie, ehrenamt]

draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
aliases:
    - /public/post/2021/polling-clerk
    - /fieldreport/polling-clerk/
    - /2021/11/02/polling-clerk
comments:
  host: social.lukas-schieren.de
  username: blog
  id: 110860284536951642

---

Alle paar Jahre wieder stehen bei uns entweder in der Kommune, im Bundesland, im Bund oder in Europa Wahlen nach demokratischen Grundsätzen an. Damit bei diesen Wahlen die demokratischen Wahlgrundsätze eingehalten werden können, ist die Demokratie auf ehrenamtliches Engagement der wählenden Bevölkerung angewiesen. Kurz um: Wahlhelfer:innen sind für das ordnungsgemäße Durchführen von Wahlen in einer Demokratie absolut systemrelevant! Dies ist ein Erfahrungsbericht nach zwei Einsätzen als Wahlhelfer (Kommunal- und Bundestagswahl) - beide Male als Erstwähler.

<!--more-->

## Warum habe ich mich überhaupt als Wahlhelfer in einen Wahlvorstand berufen lassen?

Eine Berufsschullehrkraft hat uns damals Anfang 2019 kurz vor der Europawahl dazu ermutigt, sich als Wahlhelfer:in zu melden.

Wichtig dabei zu wissen ist, dass **leider** für das Ausüben des Wahlhelfer:innen-Ehrenamtes bei der Europawahl, wie bei Bundestagswahlenauch, eine Voraussetzung **das Erreichen des Wahlberechtigen-Alter - bei Europawahlen Stand jetzt 18 Jahre** ist.

Da ich zum Zeitpunkt der Europawahl 2019 noch keine 18 Jahre alt (siehe [§ 5 Abs. 3 Satz 1 Europawahlgesetz (EuWG); Link zu gesetze-im-internet.de)](https://www.gesetze-im-internet.de/euwg/__5.html) war, konnte ich mich also leider nicht schon damals als Wahlhelfer bereiterklären. Also kam für mich erst die nächste Wahl infrage: die Kommunalwahl in Nordrhein-Westfalen im letzten Jahr.

Dabei meldete ich mich zeitnah im zweiten Quartal des Jahres bei der zuständigen kommunalen Behörde und bekundete mein Interesse, mich für die Kommunalwahl 2020 als Wahlhelfer melden zu wollen.

## Wie wird man Wahlhelfer:in?

Im Grunde gibt es drei Schritte vor der Wahl, zwei während der Wahlhandlung am Wahltag und drei Schritte nach der Wahlhandlung am Wahltag.

**Vor der Wahl:**

1. Man meldet sich über E-Mail, über Telefon oder ggf. auch mit einem Vor-Ort-Besuch bei der zuständigen kommunalen Behörde - im Regelfall das Wahlamt der jeweiligen Stadt, des jeweiligen Kreises oder der jeweiligen Gemeinde.
2. Man wartet auf Rückmeldung des Wahlamtes - unter Umständen nochmal nachfragen, wenn es kurz vor der Wahl ist und bisher nichts kam.
3. Im Grunde fehlt dann nur noch das im Regelfall postalische Ersuchen des Stadt-, Kreis- oder Gemeindeoberhaupt zur Berufung in den Wahlvorstand bestätigen.

**Während der Wahlhandlung am Wahltag:**

1. Am Wahltag selbst solltet ihr euch um 07:30 Uhr in das Wahllokal, welches in der Berufung in den Wahlvorstand steht, einfinden.
2. Dann wird gemeinsam mit den restlichen Mitgliedern des Wahlvorstandes zum einen das Wahllokal für die Wahl vorbereitet und zum anderen entschieden, wer welche Schicht übernimmt - im Regelfall gibt es eine Schicht von 08:00 Uhr bis 13:00 Uhr und von 13:00 Uhr bis 18:00 Uhr

**Nach der Wahlhandlung am Wahltag:**

1. Nach Ende der Wahlmöglichkeit am Wahltag (18:00 Uhr) finden sich alle Wahlvorstandsmitglieder im Wahllokal ein, um die Stimmzettel auszuzählen.
2. Im Anschluss an die Auszählungen übermittelt der Wahlvorstandsvorsitz telefonisch eine Schnellmeldung an das kommunale Wahlamt mit den Ergebnissen. Ganz am Ende wird das Endergebnis des Wahlbezirks des Wahllokals durch den Wahlvorstandsvorsitz an das Wahlamt übermittelt.
3. Nach der Auszählung gibt es ein Dokument, welches von den anwesenden Wahlvorstandsmitgliedern unterzeichnet werden kann, wenn Sie beim nächsten Mal auch als Wahlhelfer:in in Erwägung kommen sollen. Dadurch wird man ins Wahlhelfer:innen-Verzeichnis aufgenommen.
4. Als Letztes wird durch den Wahlvorstandsvorsitz an die restlichen Mitglieder des Wahlvorstandes das sogenannte Erfrischungsgeld, welches von Kommune, Kreis, Gemeinde unterschiedlich ausfallen kann, an die Mitglieder des Wahlvorstandes ausgezahlt und dies von diesen gegengezeichnet.

**Ein Tipp meinerseits in Bezug auf Bewerbungen:** Lasst euch den Einsatz als Wahlhelfer:in in jedem Fall durch eine Urkunde oder ein Schreiben des Wahlamtes bescheinigen, die das ehrenamtliche Engagement belegt. Die Bestätigung ehrenamtlichen Engagements dieser Art kann bei (potenziellen) Arbeitsgeber:innen positiv hervorstechen gegenüber anderen Bewerbenden.

## Kann ich empfehlen sich für das Wahlhelfer:innen-Ehrenamt zu melden?

Ja, absolut! Ich kann nur bestätigten, was damals die Berufsschullehrkraft sagte: Es ist eine Erfahrung, die man gemacht haben sollte! Man erlebt, finde ich, unsere Demokratie durch dieses Ehrenamt hautnah.

Ich habe auch schon vor der diesjährigen Bundestagswahl mein Interesse gegenüber des kommunalen Wahlamtes bekundet, dass ich auch für die Landtagswahl in Nordrhein-Westfalen im Mai 2022 gerne als Wahlhelfer dabei sein möchte - dann aber bevorzugt als Mitglied in einem Briefwahlvorstand.

Ich beabsichtige mich auch für die nächste Europawahl im Jahr 2024 & Bundestagswahl im Jahr 2025, als Wahlhelfer zur Verfügung zu stellen.

## Könnte das Interesse an Politik bei jungen Menschen unter 18 Jahren gestärkt werden, wenn die Bindung von Wahlalter und Ausübung des Wahlhelfer:innen-Ehrenamtes aufgehoben würde?

Ich persönlich denke, dass wenn die Bindung zwischen Wahlalter und Ausübung des Wahlhelfer:innen-Ehrenamtes aufgehoben würde, dies durchaus dazu beitragen könnte, das Interesse an Politik & das Verständnis der Prinzipien unserer Demokratie bei jungen Menschen zu verstärken. So könnte man es meiner Ansicht nach politisch auch mit guten Gewissens vertreten, junge Menschen ab 16 Jahren zu Wahlen in allen Ebenen (Kommune, Bundesland, Bund und EU) zuzulassen.

## Weitere Informationsseiten

- [Erklärung der Bundeswahlleitung zum Bundeswahlgesetz (BWG)](https://bundeswahlleiter.de/service/glossar/b/bundeswahlgesetz.html)
- [Erklärung der Bundeswahlleitung zur Bundeswahlordnung (BWO)](https://bundeswahlleiter.de/service/glossar/b/bundeswahlordnung.html)
- [Erklärung der Bundeswahlleitung zu Demokratie](https://bundeswahlleiter.de/service/glossar/d/demokratie.html)
- [Erklärung der Bundeswahlleitung zum Erfrischungsgeld für die Wahglhelfer:innen](https://bundeswahlleiter.de/service/glossar/e/erfrischungsgeld.html)
- [Erklärung der Bundeswahlleitung zu Wahlvorstand und -vorsitz](https://bundeswahlleiter.de/service/glossar/w/wahlvorsteher-wahlvorstand.html)
- [Reportage der Bundeszentrale für politische Bildung (Bpb) "Ein tag als Wahlhelfer"](https://www.bpb.de/dialog/europawahlblog-2014/185748/ein-tag-als-wahlhelfer-eine-reportage)
- [Wahl-ABC der Tagesschau](https://www.tagesschau.de/inland/btw21/wahllexikon/)
- [FAQ der Tagesschau zur Bundestagswahl mit dem Thema "So wird gewählt"](https://www.tagesschau.de/inland/btw21/faq-waehlen-101.html)
- [FAQ der Tagesschau zur Briefwahl](https://www.tagesschau.de/inland/btw21/briefwahl-125.html)
