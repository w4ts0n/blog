---
title: "Ein Leben als junger Mensch im 21. Jahrhundert ohne WhatsApp - ist das möglich? Ein Zwischenbericht"
date: 2021-04-09
lastmod: 2023-06-24
author: Lukas Schieren

description: Nach circa zweidreiviertel Jahren des Abwägens habe ich mich vor knapp drei Monaten dazu durchringen können, den seit dem Jahr 2014 zum US-amerikanischen Facebook-Konzern gehörenden Messenger WhatsApp vollständig von meinem Mobiltelefon zu löschen. Im Format eines Blogbeitrags will ich einen ersten Zwischenbericht darüber abgeben, wie es sich als junger Mensch im 21. Jahrhundert ohne WhatsApp lebt und wie mein (näheres) Umfeld auf die vollständige Deinstallation von WhatsApp reagiert hat. Außerdem möchte ich erklären, warum unter Datenschutzgesichtspunkten WhatsApp sehr häufig (meiner Meinung nach zurecht) in der Kritik steht.
categories: [Erfahrungsbericht]
tags:
  [privacy, WhatsApp, Threema, Signal, Datenschutz, Facebook, Zwischenbericht]

draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
aliases:
  - /public/post/2021/whatsapp-delete
  - /2021/09/04/whatsapp-delete
comments:
  host: social.lukas-schieren.de
  username: blog
  id: 110860280623458280
---

Nach circa zweidreiviertel Jahren des Abwägens habe ich mich vor knapp drei Monaten dazu durchringen können, den seit dem Jahr 2014 zum US-amerikanischen Facebook-Konzern gehörenden Messenger WhatsApp vollständig von meinem Mobiltelefon zu löschen. Im Format eines Blogbeitrags will ich einen ersten Zwischenbericht darüber abgeben, wie es sich als junger Mensch im 21. Jahrhundert ohne WhatsApp lebt und wie mein (näheres) Umfeld auf die vollständige Deinstallation von WhatsApp reagiert hat. Außerdem möchte ich erklären, warum unter Datenschutzgesichtspunkten WhatsApp sehr häufig (meiner Meinung nach zurecht) in der Kritik steht.

<!--more-->

Zuletzt möchte ich sichere WhatsApp-Alternativen aufzeigen und technische sowie organisatorische Maßnahmen zeigen, mit denen Ihr WhatsApp zumindest datenschutzkonformer betreiben könnt, solange ein Umstieg nicht gewollt ist. Dann möchte proaktiv die Lesenden zum Nachdenken über einen möglichen Wechsel von unsicheren zu sicheren Messengern anregen.

## Wie hat mein (näheres) Umfeld auf die vollständige Deinstallation von WhatsApp reagiert?

Es war und ist kein Geheimnis, dass ich sämtlicher digitaler Kommunikation, die über WhatsApp abgewickelt wurde, kritisch gegenüber stand und nach wie vor stehe. Wie ich in der kurzen Zusammenfassung eingangs schon kurz anriss, war die vollständige Deinstallation keine spontane Aktion, sondern ein über zwei Jahre andauernder Prozess. Aus diesem Grund ist es nicht verwunderlich gewesen, dass eine Person aus dem Bekanntenkreis, als ich Ihr von meinem finalen Vorhaben zum Ende des Jahres 2020 WhatsApp zu deinstallieren erzählte, mich fragte, ob ich das wirklich ernst meinen würde, da ich so etwas auch schon in der Vergangenheit angekündigt hatte und es trotzdem noch nutzte. Wie man nun sieht: Ja, ich meinte es ernst und ich bereue es nicht.
Auch führten verschiedene ehrenamtliche Verpflichtungen dazu, dass eine vollständige Trennung vom unsicheren Messenger WhatsApp nicht möglich war. Aus diesem Grund ergriff ich zunächst die organisatorische Maßnahme, die Kommunikation über WhatsApp auf ein Mimimum zu reduzieren. Als technische Maßnahme entzog ich WhatsApp alle seine Berechtigungen.

# Was & warum ist die Nutzung von WhatsApp zur digitalen Kommunikation unter Datenschutzgesichtspunkten als (sehr) kritisch anzusehen?

Inga Pöting von mobilsicher.de hat im Rahmen des Kooperationsprojekts "mobil und save" mit der EU-Initiative klicksafe in einem Video[^1] sehr gut dargestellt, welche drei Argumente gegen WhatsApp sprechen. Diese verwende ich nun auch hier, um gemeinsam mit weiteren Quellen darzulegen, was & warum die Nutzung von WhatsApp unter Datenschutzgesichtspunkten nicht empfehlenswert ist.

Häufig hört man so oder so ähnliche Aussagen, wenn Menschen auf die kritischen Datenschutzpunkte bei WhatsApp aufmerksam gemacht werden:

> _"Ich habe nichts zu verbergen - soll WhatsApp doch wissen, was ich schreibe."_

Der Umstand, dass große Technikkonzerne, wie Facebook, zu dem WhatsApp seit 2014 gehört[^2], zunächst nicht am Inhalt von Dokumenten oder Nachrichten, sondern an Metadaten interessiert sind, ist dieser, dass diese viel mehr über die Nutzenden verraten, als einem lieb ist.

Der ehemalige US-Geheimdienst-Mitarbeitende & Whistleblower Edward Snowden traf die folgende Aussage, die die obrige Aussage entkräftet:[^3]

> _"Arguing that you don't care about the right to privacy because you have nothing to hide is no different than saying you don't care about free speech because you have nothing to say.”_

## 1. Metadaten

### Was sind Metadaten?

Um zu verstehen, wie viel Metadaten über die Nutzenden aussagen können, ist es zunächst notwendig eine Definition zu haben. Der deutschsprachige Webvideoproduzent & zertifizierte Informatik-Dozent Cedric Mössner besser bekannt als "TheMorpheusVlog" traf dazu in meinen Augen in seinem am 12. Januar 2021 veröffentlichten YouTube-Video eine sehr treffende und leicht verständliche Definition - er sagte:[^4]

> _"Metadaten sind Daten, die anfallen, wenn man etwas benutzt, aber tatsächlich den eigentlichen Inhalt nicht lesen kann."_

Auch Inga Pöting von Mobilsicher.de hat eine sehr treffende und leicht verständliche Definition von Metadaten:[^5]

> _"Metadaten sind alle Daten, rund um eure Kommunikation"_

Das bedeutet, dass WhatsApp zwar zunächst nicht die Nachrichten, die über den Dienst versendet werden lesen kann, dafür aber - ja - was denn nun eigentlich?

#### Was macht WhatsApp mit diesen Daten?

Nachdem WhatsApp diese Informationen, die auch in der aktuellen Fassung der Datenschutzerklärung von WhatsApp stehen, automatisch erfasst, wertet es diese aus und erstellt Profile der Nutzenden. Eine Aussage von Hubert Jäger, Geschäftsleitung des IT-Sicherheitsunternehmen Uniscon und deutscher Sicherheitsexperte im Zusammenhang mit dem Abhörskandal aus dem Jahr 2013 untermauert diese Praxis nochmals:[^6]

> _„Zu wissen, wer wen wann und wie oft anruft oder anschreibt, enthüllt privateste Informationen. Je mehr Daten man sammelt, desto aufschlussreicher wird das Ergebnis.“_

Ich habe mal die Datenschutzerklärung von WhatsApp in der aktuell gültigen Fassung gescreenshotet und unter Datenschutzgesichtspunkten kritischen markiert:[^7]

Das bedeutet, dass WhatsApp mithilfe von Metadaten den ungefähren Tagesablauf der Nutzenden und gemeinsam mit den ungefähren Standortinformationen, die es über Standort, WLAN und mobiles Internet erhält, ein umfassendes Bewegungsprofil der Nutzenden erstellen kann. WhatsApp könnte darauf schließen, welchen Hobbys die Nutzenden nachgehen, welche medizinischen Dienstleistungen Sie in Anspruch nehmen müssen, indem Sie mutmaßen auf Grundlage der Standortdaten, zu welchen Ärzten oder anderen medizinischen Einrichtungen die Nutzenden gehen. All das sind Informationen, die in meinen Augen WhatsApp und somit Facebook nichts anzugehen haben![^8]

Denn: Sichere Alternativen, wie Threema oder Signal zeigen, wie sichere Digitalkommunikation ohne mehr als technisch notwendige Metadaten zu erheben, gut funktionieren kann.[^9]

Um zu verstehen, was beispielsweise WhatsApp oder Geheimdienste mit Metadaten alles anfangen könnten, finde ich das Zitat des ehemaligen NSA-Chefs Michael Hayden sehr wichtig - er sagte:[^10]

> _"We kill people based on Metadata."_

Mike Kuketz hat einem seiner Blogbeiträge etwas ausführlicher aufgeschlüsselt, was alles für personenbezogene Daten an WhatsApp übermittelt werden.[^11]

## 2. Unverschlüsselte Backups

Die Kommunikation zwischen zwei Gesprächspartnern ist seit 2016 zwar Ende-zu-Ende-verschlüsselt, aber nur solange, bis einer der beiden Kommunikationspartner:innen ein Backup macht. Bei Android in die Dateiablage von Google, bei iOs in die Cloud von Apple. Das bedeutet, das der Inhalt von Nachrichten auch nicht so sicher ist, wie WhatsApp es behauptet und somit von potenziellen Angreifenden, die Möglichkeit bestünde, diese Informationen abzugreifen.[^12]

Dass dies auch verschlüsselt und somit sicherer funktionieren kann, zeigt Threema seit Jahren, indem ein Backup als ein passwortgeschütztes .zip-Archiv zunächst auf dem Gerätespeicher abgespeichert werden kann. Der Threema-Nutzenden Person steht es dann offen, dieses auf externen Speichermedien zu übertragen und sichern. Ein zwingender Upload in die Cloud von großen Technikkonzernen ist nicht notwendig.[^13]

## 3. (Obligatorischer) Adressbuchabgleich

Was vielen scheinbar nicht so ganz bewusst ist, dass das gesamte(!) Adressbuch regelmäßig bei WhatsApp hochgeladen wird. Das bedeutet sowohl von den Kontakten, die WhatsApp besitzen, aber auch von denen, die es nicht haben. Informationen von (Nicht-)WhatsApp Nutzenden werden unzureichend auf den WhatsApp-Servern verschlüsselt, sodass diese entschlüsselbar und zurückführbar auf natürliche Personen sind. Es kommt noch dicker: Es muss davon ausgegangen werden, dass die Informationen, der nicht WhatsApp Nutzenden vorrausichtlicht Werbezwecken und ähnlichem ausgewertet werden.[^14] , [^15]

Welche technischen und organisatorischen Maßnahmen man ergreifen kann, damit man (künftig) die Informationen, der im Adressbuch gespeicherten, die WhatsApp nicht (mehr) nutzen, schützen kann, zeige ich am Ende.

Dass diese automatische Datenübermittlung einen Datenschutzverstoß darstellt, hat in einem Familienstreit das Amtsgericht Bad Hersfeld (nachfolgend AG Bad Hersfeld) klargestellt, wo ein geschiedenes Ehepaar den Umgang mit dem gemeinsamen Kind klären wollte, wobei das Mobiltelefon des Kindes auch Thema im Rechtsstreit wurde. Das AG Bad Hersfeld forderte die Mutter auf, dass Sie von jedem der im Adressbuch des Kindes hinterlegten Kontakte eine Einverständniserklärung einzuholen und dem Gericht vorzulegen hat, da die automatisierte Übermittlung von Adressbuchinformationen ansonsten einen Verstoß gegen das aktuelle Datenschutzrecht sei. [^16] , [^17]

Der Medienanwalt Christian Solmecke ist im Kern der selben Meinung, wie die, die in der gerichtlichen Argumentation vorgebracht wird. Auch wenn er sich frage, weil es aus den AGB nicht ersichtlich sei, dass hier Klarnamen + Telefonnummer erhoben würden. Nichts desto trotz stelle laut Solmecke die Telefonnnummer eine personenbezogene Angabe dar und sei somit schützenswert.[^18]

## 4. Potenzieller Datenaustausch von WhatsApp und Facebook

Dass Facebook auch nicht sonderlich gut mit persönlichen Informationen von Nutzenden umgeht, das zeigen zahlreiche Skandale. Dass Facebook jahrelang Daten an die britische Analysefirma Cambridge Analytica gab und diese dann Profile erstellten, die für Werbezwecke verkauft werden können, zeigt die Brisanz der Sammlung und Auswertung von Metadaten nochmal ganz enorm.[^19]

Der Bundesbeauftragte für den Datenschutz und die Informationsfreiheit (BfDI) Prof. Ulrich Kelber erteilte WhatsApp in seinem 27. Tätigkeitsbericht ab Seite 109 f., aufgrund des potenziellen Austauschs von Informationen mit dem Facebook-Konzern und deren Unternehmen eine klare Absage und stufte ihn als nicht datenschutzsicher ein.[^20] Facebook musste damals beim Kauf von WhatsApp im Jahr 2014 sich dazu verpflichten, dass mitnichten ein Datenaustausch zwischen WhatsApp und Facebook stattfinden wird. Facebook verfolgt mit dem Änderungen der Allgemeinen Geschäftsbedingungen das Ziel genau dies nun zu erzwingen, indem Sie die Nutzenden vor folgende Wahl stellen: Entweder Du gibst uns deine Daten, oder Du musst gehen![^21]

Ein Tipp meinerseits: **Lest euch mal die Allgemeinen Geschäftsbedingungen und Datenschutzerklärung von WhatsApp durch!**

# Welche sicheren WhatsApp-Alternativen gibt es?

Man mag es kaum glauben, aber tatsächlich gibt es sichere WhatsApp-Alternativen. In diesem Fall stelle ich die beiden Messenger Threema und Signal vor. Zur häufig geratenen sicheren WhatsApp-Alternativen Telegram komme ich danach.[^22]

## Signal

Signal ähnelt WhatsApp optisch sehr, wodurch Menschen, die WhatsApp gewöhnt sind, es leichter haben, sich an Signal gewöhnen zu können. Signal ist kostenlos und benötigt lediglich die Mobilfunknummer. Anders als bei WhatsApp verschafft sich Signal aber nicht Zugriff auf die Kontaktdaten, sondern mithilfe notwendiger Verschlüsselungs- und Anonymisierungsmaßnahmen werden die Mobilfunknummern auf Signal-Servern abgeglichen, um anzeigen zu können, wer Signal auch nutzt und nach diesem Abgleich sofort wieder vom Server entfernt. Darüber hinaus wird Signal von einer Stiftung finanziert und verfolgt somit keine kommerziellen Interessen. [^23]

## Threema

Der schweizerische Messenger Threema sticht damit hervor, dass für die Nutzung von Threema keine Angaben von Mobilfunknummer und/oder E-Mail-Adressen notwendig sind.[^24] Nach dem Kauf und der Installation von Threema wird ein zufälliger QR-Code generiert, der zur Verifikation und Identifikation dient. Die Threema-ID setzt sich aus zwei Komponenten, dem privaten und öffentlichen Schlüssel zusammen.[^25] Der öffentliche Schlüssel wird an den Threema-Server übermittelt, damit er die Chatnachrichten zum jeweiligen Chatpartner:in schicken kann, bleibt der private Schlüssel auf dem Smartphone, wo er entsprechend sicher abgelegt wird. Die über Threema versendeten Nachrichten werden mithilfe des öffentlichen Schlüssels verschlüsselt und mithilfe ihres privaten Schlüssels bei Ihnen wieder entschlüsselt. Man kann sich das, wie den Prozess bei der PGP-E-Mail-Verschlüsselung vorstellen.[^26]
Threema kostet im Gegensatz zu Signal einmalig(!) 3,99 EUR (€), was aber in meinen Augen völlig vertretbar ist. Darüber hinaus sind die Kommunikation über Threema, sowie die Backups verschlüsselt.[^27] [^28]
Threema ist mittlerweile clientseitig Open Source, das bedeutet, dass jede:r in die Quellen der mobilen Anwendung schauen kann und mitwirken könnte. Serverseitig ist Threema leider (noch) nicht Open Source.[^29] [^30]

## Sonderfall: Telegram

Häufig wird Telegram als sichere WhatsApp-Alternative benannt. Dies sehe ich aufgrund verschiedener Punkte nicht. Telegram hat in privaten Chats eine optionale und in Gruppenchats gar keine Ende-zu-Ende-Verschlüsselung. Der zertifizierte Informatik-Dozent Cedric Mössner hat im Rahmen einer Netzwerkanalyse herausgefunden, dass Telegram seine Netzwerkverschlüsselung wohl selbst programmiert und implementiert habt. Das birgt enorme Sichereheitsrisiken, so Mössner. Darüber hinaus wird laut der Datenschutzerklärung von Telegram sämtliche Kommunikation, ausgenommen der geheimen Chats auf Telegram-Servern gespeichert.[^31] [^32] [^33] [^34] [^35]
Eine umfassende laufend aktualisierte Gegegnüberstellung von Messengers hat Kuketz-Blog aufgestellt, die in an dieser Stelle verlinken möchte.[^36]

## Matrix (Auf Hinweis eines Lesenden am 11.04.21 ergänzt)

Grundsätzlich stellt auch Matrix eine WhatsApp-Alternative dar, die man sogar - falls gewünscht - selbst betreiben kann. Jedoch stellt die wenig benutzerfreundliche Oberfläche von Matrix - insbesondere für nicht technikaffine Menschen - ein Problem dar. Darüber hinaus ist der Administrationsaufwand - sofern man es selbst betreibt - auch nicht zu unterschätzen. Deswegen hatte ich es in der ursprünglichen Form des Blogbeitrags nicht als sichere WhatsApp-Alternative mit aufgeführt.
Vor dem Hintergrund, dass sichere Kommunikation auch im Bereich der Kirchen gewährleistet werden sollte, stellt der Linux User im Bereich der Kirchen e. V. (LUKI e. V.) für Kirchengemeinden und -kreise in ganz Deutschland eine Matrix-Instanzunter https://synod.im/ (externer Link) bereit.[^37]

# Welche technischen & organisatorischen Maßnahmen (TOMs) kann man ergreifen, wenn WhatsApp aus Gründen leider noch notwendig sein sollte, um die personenbezogenen Informationen der im Adressbuch gespeicherten Kontakte zu wahren?

Wenn es aus Gründen nicht möglich ist, WhatsApp zu deinstallieren, dann kann man bis zum vollständigen Übergang auf sichere Messenger, technische und organisatorische Maßnahmen treffen.

## Technische Maßnahmen

- WhatsApp ins Arbeitsprofil von Android laden (Neuinstallation notwendig). Unter Android bietet sich die App "Shelter" aus dem F-Droid Store[^38] an.
- Kontakte von denen man die Einverständniserklärung hat, in die Kontakte im Arbeitsprofil übertragen. So werden nur Informationen von diesen an WhatsApp übermittelt.

## Organisatorische Maßnahmen

- Kommunikation über WhatsApp auf ein Minimum herunterfahren
- Einholung von Einverständniserklärungen der Kontakte
- Bekannten- und Freundeskreis sowie Familienmitglieder auf sichere WhatsApp-Alternativen aufmerksam machen und diese dabei unterstützen, kollektiv umzuziehen

# Weitere Informationen

Welche weiteren (technischen) Maßnahmen unter Android sinnvoll sein könnten, hat eine Person aus meinem Bekanntenkreis in einem seiner Blogbeiträge[^39] erklärt.
An dieser Stelle möchte ich auf verschiedene Beiträge des Blogs von Mike Kuketz verweisen: https://www.kuketz-blog.de/mustertext-anregung-zum-umzug-whatsapp-zu-signal-threema/

# Quellen:

[^1]: Inga Pöting von mobilsicher (Veröffentlicht in 2020): 3 Argumente gegen WhatsApp | App-Tipps: Signal & Threema | mobil & safe. Online im Internet unter https://devtube.dev-wiki.de/videos/watch/3e047184-54b9-487b-b538-1e4e2dd4db18 (Aufgerufen am 31.03.2021)
[^2]: Ben Miller (2014): Facebook kauft WhatsApp für 16 Millarden US-Dollar. Online im Internet unter https://www.giga.de/unternehmen/whatsapp-inc/news/facebook-kauft-whatsapp-fuer-16-milliarden-us-dollar/ (Aufgerufen am 05.04.2021)
[^3]: https://www.reddit.com/r/IAmA/comments/36ru89/just_days_left_to_kill_mass_surveillance_under/ (Aufgerufen am 05.04.2021)
[^4]: Cedric Mössner (Veröffentlicht am 12.01.2021): Die Datenvon WhatsApp - Was WIRKLICH möglich ist (Metadaten). Online im Internet unter https://www.youtube.com/watch?v=rO37rz1dhkQ (Aufgerufen am 05.04.2021)
[^5]: Inga Pöting von mobilsicher (Veröffentlicht in 2020): 3 Argumente gegen WhatsApp | App-Tipps: Signal & Threema | mobil & safe. Online im Internet unter https://devtube.dev-wiki.de/videos/watch/3e047184-54b9-487b-b538-1e4e2dd4db18 (Aufgerufen am 05.04.2021)
[^6]: cp (Veröffentlicht am 23.09.2013): Abhörskandal: Metadaten oft aufschlussreicher als der eigentliche Inhalt. Online im Internet unter https://www.datensicherheit.de/abhoerskandal-metadaten-inhalt (Aufgerufen am 05.04.2021)
[^7]: WhatsApp Inc. (Zuletzt aktualisiert am 04.01.2021): WhatsApp Datenschutzrichtlinie. Online im Internet unter https://www.whatsapp.com/legal/updates/privacy-policy/?lang=de&_fb_noscript=1 (Aufgerufen am 05.04.2021)
[^8]: Emily Birnbaum (Veröffentlicht am 17.12.2019): Twitter Thread zum Thema Facebook und Metadaten. Online im Internet unter https://twitter.com/birnbaum_e/status/1207000504129245184 (Aufgerufen am 05.04.2021)
[^9]: Jonas Bickelmann (2021): Was machen Signal oder Telegram besser?. Online im Internet unter https://www.tagesspiegel.de/wirtschaft/alternativen-zu-whatsapp-was-machen-signal-oder-telegram-besser/26845874.html (Aufgerufen am 05.04.2021)
[^10]: Lee Ferran (2014): Ex-NSA Chief: 'We Kill People Based on Metadata'. Online im Internet unter https://abcnews.go.com/blogs/headlines/2014/05/ex-nsa-chief-we-kill-people-based-on-metadata (Aufgerufen am 05.04.2021)
[^11]: Mike Kuketz (2019): Welche personenbezogenen Daten von einem Dritten übermittelt WhatsApp. Online im Internet unter https://www.kuketz-blog.de/welche-personenbezogenen-daten-von-einem-dritten-uebermittelt-whatsapp/ (Aufgerufen am 05.04.2021)
[^12]: Mike Kuketz (2018): WhatsApp-Backups liegen unverschlüsselt in der Google Cloud. Online im Internet unter https://www.kuketz-blog.de/whatsapp-backups-liegen-unverschluesselt-in-der-google-cloud/ (Aufgerufen am 05.04.2021)
[^14]: Steve Haak (2013): Facebook kauft WhatsApp für 16 Millarden US-Dollar. Online im Internet unter https://www.golem.de/news/adressbuchabgleich-whatsapp-wird-von-datenschuetzern-kritisiert-1301-97225.html (Aufgerufen am 05.04.2021)
[^15]: https://www.klicksafe.de/themen/kommunizieren/whatsapp/probleme-mit-dem-whatsapp-messenger (Aufgerufen am 05.04.2021)
[^16]: https://www.rv.hessenrecht.hessen.de/bshe/document/LARE190000029 (Aufgerufen am 05.04.2021)
[^17]: https://dejure.org/dienste/vernetzung/rechtsprechung?Text=F%20111/17 (Aufgerufen am 05.04.2021)
[^18]: Christian Solmecke (2017): Weitergabe der WhatsApp-Kontaktdaten ist illegal - droht eine Abmahnwelle? Online im Internet unter https://www.wbs-law.de/it-und-internet-recht/datenschutzrecht/urteil-weitergabe-der-whatsapp-kontaktdaten-ist-illegal-droht-eine-abmahnwelle-22712/ (Aufgerufen am 07.04.2021)
[^19]: Netflix (Veröffentlicht in 2019): Cambridge Anallyticas großer Hack. Online im Internet unter https://www.netflix.com/de/Title/80117542 (Aufgerufen am 05.04.2021)
[^20]: Prof. Ulrich Kelber (2019): 27. Tätigkeitsbericht zum Datenschutz 2017 - 2018. Online im Internet unter https://www.bfdi.bund.de/SharedDocs/Publikationen/Taetigkeitsberichte/TB_BfDI/27TB_17_18.html?nn=5217016 (Aufgerufen am 06.04.2021)
[^21]: Dieter Petereit (Veröffentlicht am 01.03.2021): "Friss oder stirb": Bundeskartellamt kritisiert WhatsApp-Ultimatum. Online im Internet unter https://t3n.de/news/friss-stirb-bundeskartellamt-1363012/ (Aufgerufen am 05.04.2021)
[^22]: WhatsApp Inc. (Zuletzt aktualisiert am 04.01.2021): WhatsApp Legal. Online im Internet unter https://www.whatsapp.com/legal/?lang=en (Aufgerufen am 05.04.2021)
[^23]: Kate O'Flaherty (2021): How To Use Signal: The Brilliant WHatsApp Alternative. Online im Internet unter https://www.forbes.com/sites/kateoflahertyuk/2021/01/11/how-to-use-signal-the-awesome-whatsapp-alternative/ (Aufgerufen am 05.04.2021)
[^24]: Threema GmbH: What makes Threema special? Online im Internet unter https://threema.ch/en/faq/special_because (Aufgerufen am 05.04.2021)
[^25]: Threema GmbH: What is a Threema ID? Online im Internet unter https://threema.ch/en/faq/threema_id (Aufgerufen am 05.04.2021)
[^26]: https://threema.ch/en/faq/threema_id
[^27]: Google Inc. (Zuletzt aktualisiert am 17.03.2021): Threema. Secure and private Messenger. Online im Internet unter https://play.google.com/store/apps/details?id=ch.threema.app (Aufgerufen am 05.04.2021)
[^28]: Apple Inc. (Zuletzt aktualisert in 2021): Threema. Sicherer Messenger. Online im Internet unter https://apps.apple.com/de/app/threema/id578665578?ign-mpt=uo%3D4 (Aufgerufen am 05.04.2021)
[^29]: Threema GmbH: Open Source What's better than trust? Transparency. Online im Internet unter https://threema.ch/en/open-source (Aufgerufen am 03.04.2021)
[^30]: Catalin Cimpanu (veröffentlicht am 04.09.2020): Threema E2EE chat app to go 'fully open source' within months. Online im Internet unter https://www.zdnet.com/article/threema-e2ee-chat-app-to-go-fully-open-source-within-months/ (Aufgerufen am 06.04.2021)
[^31]: Cedric Mössner (2020): Die Probleme von TELEGRAM. Online im Internet unter https://www.youtube.com/watch?v=3JK_0_gZGMA (07.04.2021)
[^32]: Telegram: MTProto Mobile Protocol. Online im Internet unter https://core.telegram.org/mtproto (Aufgerufen am 03.04.2021)
[^33]: Telegram: Warum nicht einfach alle Chats "geheim" machen?". Online im Internet unter https://telegram.org/faq#f-warum-nicht-standardmig-alle-chats-geheim (Aufgerufen am 02.04.2021)
[^34]: Mike Kuketz (Veröffenlicht am 08.03.2020): Telegram: "Sicherheit" gibt es nur auf Anfrage - Messenger Teil 3. Online im Internet unter https://www.kuketz-blog.de/telegram-sicherheit-gibt-es-nur-auf-anfrage-messenger-teil3/ (Aufgerufen am 07.04.2021)
[^35]: Markus Beckedahl (Veröffenlicht am 20.11.2020): Telegram ist nicht so sicher, wie das Image verspricht. Online im Internet unter https://netzpolitik.org/2020/bits-telegram-ist-nicht-so-sicher-wie-das-image-verspricht/ (Aufgerufen am 07.04.2021)
[^36]: Mike Kuketz (Zuletzt aktualisiert am 07.04.2021): Messenger-Matrix. Online im Internet unter https://media.kuketz.de/blog/messenger-matrix/messenger-matrix.html (Aufgerufen am 08.04.2021)
[^37]: Johannes Brakensiek (Veröffentlicht am 18. März 2020; Zuletzt aktualisiert am 21.08.2020): LUKi stellt frei zugänglichen Messenger-Dienst „synod.im“ zur Verfügung. Online im Internet unter https://luki.org/2020/03/luki-stellt-frei-zuganglichen-messenger-dienst-synod-im-zur-verfugung/ (Aufgerufen am 11.04.2021)
[^38]: Petercxy (Veröffentlicht in 2020): Shelter. Online im Internet unter https://f-droid.org/en/packages/net.typeblog.shelter/ (Aufgerufen am 06.04.2021)
[^39]: Dirk Grünhagen (Veröffenlicht am 19.03.2021): Privacy unter Android (fast) ohne großen Aufwand. Online im Internet unter https://blog.dgruen.de/post/20210320_privacyonandroidfastohneaufwand/ (Aufgerufen am 08.04.2021)
