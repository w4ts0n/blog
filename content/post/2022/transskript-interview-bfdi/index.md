---
title: "Transkript: Interview mit Bundesbeauftragten für den Datenschutz und die Informationsfreiheit (BfDI) Prof. Ulrich Kelber zum Fediverse"
description: "Transskript zu einem Interview"
date: "2022-08-24"
author: "Lukas Schieren"
hidden: true
menu:
  about:
    name: Fediverse - können soziale Netzwerke losgelöst von kommerziellen Interessen und im Einklang mit europäischen Gesetz funktionieren?
    title: Fediverse - können soziale Netzwerke losgelöst von kommerziellen Interessen und im Einklang mit europäischen Gesetz funktionieren?
    url: post/2022/fediverse
aliases:
  - fediverse/transskript-interview-bfdi
  - /public/post/2022/transskript-interview-bfdi/
  - /public/post/2022/fediverse/transskript-interview-bfdi
  - /2022/08/24/Transskript-bfdi-interview
---

**Titel:** Interview mit Bundesbeauftragten für den Datenschutz und die Informationsfreiheit (BfDI) Prof. Ulrich Kelber zum Fediverse

**Disclaimer:** Aus Gründen der Barrierefreiheit wurde das Interview mit Untertiteln versehen.

**Lukas Schieren:** Vielen Dank Herr Kelber, dass Sie sich die Zeit genommen haben für ein Interview zum Thema Fediverse!

**BfDI Kelber:** Gerne, vielen Dank für Ihr Interesse am Thema.

**Lukas Schieren:** Wie Sind Sie persönlich auf Mastodon aufmerksam gewordne und was waren Ihre Beweggründe, einen privaten Mastodon-Account zu betreiben?

**BfDI Kelber:** Also ich bin Informatiker, d.h. ich interessiere mich ohnehin auch persönlich für viele Entwicklungen: technischer Art, technisch-rechtlicher Art. Ich war vorher als Staatssekretär ja auch schon für Datenschutz zuständig und natürlich ist mir die Idee des Fediverse schon immer aufgefallen und das war ein Bereich, den ich auch für mich entschlossen habe und deswegen war es folgerichtig auf einem solchem Medium ebenfalls aktiv zu werden.

**Lukas Schieren:** Was war und ggf. ist immer noch ihre Motivation, Mastodon als Fediverse-Dienst für Bundesbehörden bereitzustellen?

**BfDI Kelber:** Das Fediverse hat natürlich noch mehr Vorteile als nur den Datenschutz. Die Unabhängigkeit, die Werbefreiheit kann man an der Stelle dazu melden und als Datenschutzbeauftragter müssen wir natürlich sehr korrekt sein bei den Wegen unserer Kommunikation. Es ist bekannt, dass wir mit einigen der Geschäftsmodelle der großen Internetkonzerne, große rechtliche Probleme haben das gerne durchsetzen wollen, dass das anders passiert und um die Kommunikation auf einem Medium anzuschieben bei der wir sagen, hier passiert es von der Datenschutzseite richtig, haben wir in der Tat wir uns überlegt, wir werden ein eigenes Konto einrichten und das tun wir dann, um es möglichst gut unter Kontrolle zu haben, als Bundesbehörde, nicht auf der Instanz eines Dritten, sondern eine eigene Instanz eingerichtet, die offen ist für Bundesbehörden, für Landesdatenschutzbeauftragten und für Parlamente.

**Lukas Schieren:** Also so wie die Berliner Kolleg\*innen dieses Angebot bereis wahrgenommen haben, richtig?

**BfDI Kelber:** Die Berliner Kolleg:innen haben gesagt, sie sind zufrieden mit der ersten Resonanz, die Sie haben seit auf den Account gegangen sind. Die Kolleg:innen in Baden-Württemberg haben ja ähnlich lange, wie wir, eine eigene Instanz (bawü.social) mit ihrem eigenen Account (@lfdi) geschaffen und und seiner einiger Zeit ist jetzt auch die Datenschutzkonferenz (DSK), also der Zusammenschluss der Datenschutzbehörden des Bundes und der Länder auf unserem Instanz aktiv.

**Lukas Schieren:** Wie wird ihr Fediverse-Angebot von den Bundesbehörden bisher angenommen? Sind die Rückmeldungen eher positiv oder negativ?

**BfDI Kelber:** Natürlich ist das intern erst mal eine zusätzliche Aufgabe, insbesondere, wenn man eine eigene Instanz durchführt und wir mussten natürlich sehr genau sein, bei der Sicherheit des Aufsetzens, bei der Datenschutz-Folgeabschätzung (DSFA), bei der Klärung der Auftragsdatenverarbeitung mit unserem Provider. Wir glauben, dass wir da auch Benchmarks gesetzt haben.

In der Arbeit der Pressestelle, von den Kolleg\*innen die mich unterstützen. Es gibt ja nicht nur, dass ich selber dort Dinge schreibe, sondern viel, das ist dann mit „/ÖÄ“ für Öffentlichkeitsarbeit gekennzeichnet, sind Kolleg:innen von mir.

Es ist eine zusätzliche Arbeit, aber natürlich nicht beliebig mehr als wenn Sie schon auf anderen sozialen Medien vorher aktiv waren, was viele Bundesbehörden bereits sind.

**Lukas Schieren:** Was haben Sie im konkreten an der Nutzung von Facebook, Instagram oder Twitter auszusetzen?

**BfDI Kelber:** Ich will bewusst zwei Dinge von einander trennen, das muss ich auch machen: Ich habe die Entscheidung auf Twitter aktiv zu sein und auf Facebook nicht, schon vor meiner Zeit als Bundesdatenschutzbeauftragter getroffen.

Weil als BfDI muss ich sehr klare Kriterien erfüllen bevor ich auch zu einem Produkt etwas sage. Wir haben uns öffentlich dazu geäußert, dass sogenannte (sog.) „Fanpages“ oder heute nur noch „Pages“ auf Facebook durch Bundesbehörden aus unserer Sicht für Bundesbehörden nicht rechtskonform zu betreiben sind, weil es hier eine geteilte datenschutzrechtliche Verantwortung gibt.

Allein das ist schon einer der Aspekte und die Behörde keineswegs tatsächlich die Verantwortung, die Sie dort hat, gegenüber den Nutzer\*innen auch erfüllen kann. Sie hat gar nicht genügend Informationen wie Daten verarbeitet werden. Und nach unserem Kenntnisstand, und da muss ich nur in die Datenschutzbestimmungen, die Facebook in Europa einsetzt, hineinschauen: Werden Daten erhoben, von der ich der Meinung bin, dass eine Bundesbehörde Sie nicht unterstützen sollte, dass sie erhoben werden bei der Nutzung von Informationsangeboten.

**Lukas Schieren:** Inwiefern haben Bundesbehörden in Bezug auf soziale Netzwerke eine Vorbildfunktion?

**BfDI Kelber:** Behörden, also öffentliche Hand, hat ohnehin eine besondere Verantwortung, das alles was sie macht rechtskonform ist und insbesondere natürlich rechtskonform dort, wo es Gesetze sind, die man selber mitentwickelt hat Deutschland war ja nun eines der Unternehmen… Entschuldigung Deutschland war eines der Staaten, die ja Vorbild mit ihrem Recht waren für die europäische Datenschutz-Grundverordnung (DSGVO).

Nur mit der Argumentation man muss da sein, wo die Bürger:innen sind, kann man nicht jedes Handeln rechtfertigen. Mir geht es auch nicht darum, solche Social Media Auftritte zu kanalisieren, zu untersagen, sondern mir geht es darum, dass die Anbieter:innen solcher Services rechtskonform handeln, europäisches Recht einhalten.

Da sind wir skeptisch, dass es an diesen Stellen tatsächlich passiert, deswegen sind wir da hinterher.

Es war übrigens nicht das erste Schreiben, schon meine Vorgängerin hatte 2016, zusammen mit der Datenschutzkonferenz (DSK) hierzu Feststellungen getroffen. Died wurden 2019 noch einmal bekräftigt und im letzten Jahr haben wir gesagt:

„So wir haben auch zugeschaut, ihr habt euch ja auch bemüht als Bundesbehörden mit Facebook zu einer anderen Vereinbarung zu kommen, aus unserer Sicht ist das bisher gescheitert. Alles was wir sehen ist nicht ausreichend, d.h. wir müssen jetzt ankündigen, dass wir zu einem bestimmen Zeitpunkt auch von den Rechten, die wir haben als Aufsichtsbehörde, im Artikel 58 der DSGVO steht drinnen, dass wir verwarnen können, dass wir anweisen und das wir untersagen können.

Das wir auch aus unserer Sicht prüfen von welcher dieser Maßnahmen wir Gebrauch machen.“

Das ist ein Prozess bei dem wir auch relativ weit fortgeschritten sind jetzt.

**BfDI Kelber:** Ich will natürlich nicht in alles Einblick geben, nun haben Sie bitte Verständnis, bei aller Transparenz, die wir ja auch als zuständige auch für Informationsfreiheit vorantreiben. Aber das erste was sie dann machen, ist nochmal eine Bestandsaufnahme – wer macht was?

Das zweite ist, hat es Angebote gegeben von Facebook, rechtliche Veränderung – ja, nein?

Wie sind bestimmte Dinge, die vielleicht angeboten werden zu bewerten. Und dann muss man eine Auswahl machen – was auf jeden Fall ganz klar sein muss, ist das, das was wir tun hieb- und stichfest sein muss, weil wir werden natürlich erwarten, dass man unsere Anweisungen oder Untersagungen nicht einfach klaglos zur Kenntnis nehmen wird, sondern das man sie vor Gericht anfechten wird und dann müssen wir uns darauf einstellen mit den teuersten Rechtsanwaltskanzleien der Bundesrepublik im Klinisch zu liegen. Also wir müssen uns gut vorbereiten.

**Lukas Schieren:** Also wie das Bundesinnenministerium bei ihrer Anweisung nach IFG?

**BfDI Kelber:** Es war keine Anweisung nach Informationsfreiheitsgesetz (IFG), weil wir leider beim Informationsfreiheitsgesetz nicht anweisen dürfen, aber wir der Meinung, dass die Art und Weise, wie das Bundesinnenministerium (BMI) das Informationsfreiheitsgesetz auslegt, das ein Verstoß gegen die Datenschutz-Grundverordnung ist und daher haben wir eine Weisung nach Datenschutz-Grundverordnung erteilt.

Und in der Tat, hat das BMI diese vor Gericht angefochten, dass wussten wir vorher, weil das war Teil unserer Besprechungen als wir gesagt haben „wir wollen handeln.“. Ich finde es durchaus in Ordnung, dass Sie das auch gerichtlich überprüfen lasst.

Und dort haben wir es in der Tat mit einer renomierten Rechtsanwaltskanzlei auf der anderen Seite zu tun und werden sehen, wie das Ganze ausgeht.

Es hat ein erstinstanzliches Urteil gegeben. Das war aus unserer Sicht nicht überzeugend, so dass wir dort auch in Revision gegangen sind.

**Lukas Schieren:** Glauben Sie, dass das Fediverse-Projekt auch nach Ihnen als BfDI weitergeführt wird?

**BfDI Kelber:** Der oder die Bundesdatenschutzbeauftragte wird vom Deutschen Bundestag auf fünf Jahre erst mal gewählt – ist ja völlig unabhängig an der Stelle. Ich habe das öffentlich schon mal gesagt, ich würde mich freuen eine zweite Amtszeit zu bekommen, es wäre aber auch automatisch meine letzte.

Von daher: Spätestens 2029 stellt sich die Frage. Das ist allerdings auch noch eine Menge Rheinwasser das da runterfließt – digital gesehen.

Aber diese Idee ist ja nicht nur mit verbunden, sie ist ja auch Teil der DNA der Menschen, die beim BfDI arbeiten und von daher bin ich zuversichtlich, dass meine Nachfolger\*innen werden sicherlich nicht datenschutzfreundliche Lösungen als erstes einstampfen.

**Lukas Schieren:** Was haben Sie als BfDI hinsichtlich des Fediverse für 2022 geplant?

**BfDI Kelber:** Der Punkt ist, unser Ziel ist natürlich nicht, Services als BfDI für den ganzen Bund zu betreiben. Dafür gibt es andere Institutionen auf der Bundesebene, wie zum Beispiel das Informationstechnikzentrum Bund (ITZBund).

Wir wollten zeigen, dass man das mit vergleichsweise einfachen Aufwand durchführen kann und wollen dazu ermuntern solche Wege auch zu gehen.

Und das ist ja durchaus an anderen Stellen erfolgreich. Ich will das mal an einem anderen Beispiel nennen:

Das eben z. B. die Messenger in den Bundesbehörden, in der Bundeswehr verwendet werden, dass das nicht proprietäre Ansätze sind dass hier auf Open Source aufgesetzt wird.

Dass der neue Messenger, der im Gesundheitssektor aufgebaut wird, für eine vertrauliche Kommunikation, erst mal zwischen Leistungserbringern, also Ärzte und Krankenhäuser, später aber auch Patient\*innen und Ihren Ärzte, das auch der auf Open Source Basis angesetzt wird.

So versuchen wir Gedankengänge reinzubringen und das gleiche machen wir halt auch beim Fediverse.

Das hier eine Variante drinnen ist, dass man das im ersten [eine] Schmeckprobe machen kann und dann für mehr Ideen, Gewinn machen kann. Wir sind als Behörde nicht eine die selber so super aktiv auf sozialen Medien unterwegs sein muss.

Wir versuchen unsere Informationen natürlich in anderer Form auch an die Leute zu bringen.

Von daher, ob wir in 2022 selber noch in einem anderen Bereich aktiv werden, das kann ich noch nicht sagen.

Hat sicherlich nicht oberste Priorität.

**Lukas Schieren:** Was würden Sie Vereinen, Organisationen, Unternehmen, Landes- und Kommunalbehörden mitgeben bei der Frage ob sie im Fediverse aktiv werden sollten?

**BfDI Kelber:** Naja, ich weiß wie abhängig man auch von Reichweite ist und dass das eine der Gründe ist, warum man als, gerade auch als Privater, als kleiner Ehrenamtler natürlich auch in die großen Netzwerke geht.

Man muss wissen, dass man dort eben diesen Geschäftsmodellen unterliegt. Und je nach den Informationen, die man auch vorhält, natürlich viel über seine Nutzer:innen an diese großen Internetkonzerne gibt. Aber das mindeste was man tun sollte, wäre auch auf datenschutzfreundlichen Alternativen unterwegs zu sein. Das ist mit vergleichsweise geringen Aufwand zu bewerkstelligen und man kann weitgehend die gleichen Informationen zur Verfügung stellen.

Also keinen exklusiven Zugang nur über datenschutzproblematische Netzwerke, sondern eben auch über das Fediverse.

Information 1: Dieses Interview mit dem Bundesbeauftragten für den Datenschutz und die Informationsfreiheit (BfDI) Prof. Ulrich Kelber wurde am 7. März 2022 geführt.

Information 2: Für das Interview und dessen NAchbereitung wurde ausschließlich auf freie und quelloffene (Open Source) Softwareprojekt[e] gesetzt. Aufnahme: OBS Projekt, Videokonferenz: BigBlueButton via Senfcall, Videoschnitt: kdenlive

---

Das Interview in Audiovisueller Form ist unter folgendem Link abrufbar: https://tube.tchncs.de/w/d38ZuevUdGVqqiJqetsFE7

---
