﻿---
title: "Interview mit Stephanie Henkel von dresden.network"
description: "Interview"
date: "2022-08-24"
lastmod: "2023-06-24"
author: "Lukas Schieren"
menu:
  about:
    name: Fediverse - können soziale Netzwerke losgelöst von kommerziellen Interessen und im Einklang mit europäischen Gesetz funktionieren?
    title: Fediverse - können soziale Netzwerke losgelöst von kommerziellen Interessen und im Einklang mit europäischen Gesetz funktionieren?
    url: post/2022/fediverse
aliases:
  - fediverse/interview
  - /public/post/2022/interview/
  - /public/post/2022/fediverse/interview
  - /2022/08/24/fediverse-interview
comments:
  host: social.lukas-schieren.de
  username: blog
  id: 110860294170545477  
---

**Lukas Schieren:** Wie bist Du persönlich auf das Fediverse aufmerksam geworden?

**Stephanie Henkel:** Das erste Mal vom Fediverse gehört habe ich 2019 bei den Hochschulpirat:innen. Dort hat mich vor allem Dani vom Fedi überzeugt, er war es auch, der den ersten Vortrag über das Fediverse gehalten hat, den ich je gehört habe.
Bis ich es selbst getestet habe, hat es aber ein wenig gedauert, weil ich mich lange nicht richtig für Social Media begeistern konnte. Ich bin im Februar 2019 in die Hochschulgruppe gekommen und kurz darauf auch Mitglied der Piraten Dresden und in den Vorstand gewählt worden. Nach einer Überwindungsphase, habe ich dann doch begonnen Social Media zu nutzen.

Damals hatte ich bereits Twitter, aber nur als lesender Account. Da ich wegen des Vorstandsamtes dann doch meine öffentliche Sichtbarkeit erhöhen wollte und deshalb mit Social Media anfing, dachte ich mir, wenn ich schon mit dieser Art der Öffentlichkeitsdarstellung anfange, dann auch dort, wo es cool ist. Im Oktober 2019 habe ich deshalb ein Profil auf dem Mastodon-Server dresden.network eröffnet und war schnell verliebt. Nach und nach habe ich dann auch angefangen, den Friendica-Account der Piraten Dresden mit zu betreuen und habe mittlerweile selbst auch noch Konten bei PeerTube, Funkwhale und Pixelfed.

**Lukas Schieren:** Was macht das Fediverse für dich so besonders? Was sind aus deiner Sicht Stärken aber auch Schwächen des Fediverse?

**Stephanie Henkel:** Besonders ist aus rein technischer Sicht der Aufbau. Dass viele verschiedene Dienste einfach miteinander reden können und es dann auch noch in diesen einzelnen Diensten wiederum verschiedene Instanzen bzw. Server gibt, welche miteinander verbunden sind, ist ein guter Beweis, dass Dezentralität funktionieren kann. Die Möglichkeit, selbst einen Server zu betreiben und unabhängig von der Gunst großer Plattformen zu sein (ich erinnere an dieser Stelle immer gern an die Blockwellen auf Twitter, die auch große Accounts wie Ende Gelände Deutschland betreffen), ist genau das, was ich so toll am Fediverse finde. Eine weitere große Stärke aus technischer Sicht sind für mich auch die vielen Funktionen und Möglichkeiten, die das Fediverse bietet, welche ein respektvolles Miteinander ermöglichen. So gibt es bei vielen Plattformen die Möglichkeiten von Bildbeschreibungen und Content Warnings, also Inhaltswarnungen, bei denen nur die Schlagworte angezeigt werden und Beiträge, die zum Beispiel Gewalt, sexuelle Inhalte oder das Thema Nahrung beinhalten, aufgeklappt werden müssen. Auch finde ich es sehr gut, dass es auf der viel genutzten Plattform Mastodon nicht die Möglichkeit gibt ungefragt Personen in Fotos zu markieren oder zitierend Beiträge zu teilen. So wird ein "übereinander statt miteinander Reden" schwieriger gemacht.

Allgemein ist der Ton, auch nach der letzten großen #neuhier-Welle, im Fediverse immer noch sehr angenehm. Klar, es gibt auch laute Menschen, die nur provozieren wollen, aber die haben nicht lange Spaß im Fediverse, weil auf sie oft nicht eingegangen wird. Ich habe es auf unserem eigenen Server erlebt, wie viele Leute auch erst richtig ankommen mussten und am Anfang mit ihren Gewohnheiten von Twitter noch etwas schroff waren und sich dann sehr schnell durch die erhöhte Zeichenanzahl und den allgemeinen Umgangston gewandelt haben. Viele davon sind auch geblieben und bereichern jetzt unsere lokale Timeline.

Aber natürlich sind nicht alle geblieben und das ist vermutlich eine ganz gute Stelle, um auf die Schwächen einzugehen. Fangen wir wieder von der technischen Seite an: Es ist für technikfernere Leute schlicht überfordernd, was wir an Möglichkeiten im Fediverse haben. Zwar gibt es mittlerweile viele Anleitungen aus der Community, aber die behandeln entweder nur Teilaspekte oder sind sehr grob oder einfach schlicht so lang, dass ein/e Nutzer:in, die eh nicht viel Interesse an Technik hat, sich das nicht alles durchlesen will. Und es gibt am Anfang so viele Fragen: Welchen Dienst will ich nutzen? Welche App brauche ich? Wo eröffne ich ein Konto? Wie finde ich Leute? Diese Fragen können abschrecken. Auch können die allgemeinen Ansprüche, wie eben auf Content Warnings zu achten, auch erstmal sehr überfordernd sein. Und wie überall in "eingeschworenen Gemeinschaften", gibt es dann manchmal Leute, die eher belehren als einen freundlichen Hinweis geben und das kann demotivierend.

Auch nicht zu unterschätzen ist, dass Leute, die neu hier sind, oft schon ein "Social Media Leben vor dem Fediverse" hatten. Es fehlen ihnen oft also die Leute, die sie bereits kennen. Vor allem, die großen Accounts. Da es im Fedi relativ egal ist, wie viele Menschen dir folgen und es eher ein gegenseitiger Austausch ist, ist das oft eine Umstellung. Obwohl ich das mit dem Austausch definitiv als Vorteil sehe. Ich stelle oft Fragen an die Leute und bekomme so viele Antworten. Genau diese Interaktion ist, soweit ich das einschätzen kann, was dann doch so viele der neueren Leute hier gehalten hat.
Ich liebe dieses Miteinander.

**Lukas Schieren:**: Wie viel Aufwand ist es, pro Woche oder Monat, eine (regionale) Mastdon-Instanz zu moderieren?

**Stephanie Henkel:** Wie gesagt, bin ich noch nicht so lange Moderatorin und es gab am Anfang, als es gerade die vielen Neuanmeldungen gab, ein wenig mehr zu tun, als jetzt. Ich teile mir den Job mit Markus, der auch Admin bei uns auf dem Server ist. Er hat vor allem bei den Neuanmeldungen ein Auge drauf, dass da keine zwielichtigen Accounts dabei sind.

Bei den jetzt insgesamt 630 Accounts auf unserem Server werden nur 138 Accounts als aktiv gewertet und da sind bereits einige Funktions-Accounts dabei, also Accounts von verschiedenen Gruppen und Stadtteil-Projekten in Dresden, die tendenziell seltener Vorkommnisse melden. Die meisten Meldungen erhalten wir von Privatpersonen, die in unangenehme Situationen geraten sind. Aber wir haben zum Glück sehr selten Meldungen und dann in der Regel mit Accounts außerhalb unseres Servers.

Die letzte größere Aufgabe ist das Moderieren der Trends und das dauert je nachdem, ob ich mich dann noch unter Beiträgen in die Diskussionen einlesen möchte, weil es so spannend ist, etwa 5 - 40 min am Tag.

**Lukas Schieren:** Sollten profitorientierte soziale Netzwerke wie Facebook oder Twitter vergesellschaftet werden?

**Stephanie Henkel:** Ich glaube nicht, dass eine Vergesellschaftung oder Verstaatlichung dieser kaputten Systeme wirklich etwas bringt.
Ja, natürlich ist es nicht gut, wenn irgendwelchen Milliardären große Kommunikationsplattformen gehören und sie durch Werbung, Datensammelei und -verarbeitung und Sonderdienste für Leute, die es sich leisten können, Geld verdienen. Aber das Problem ist doch, dass die Plattformen überhaupt diese Möglichkeiten bieten, durch solche Methoden Geld mit den Nutzenden zu verdienen.

Ich glaube nicht, dass durch eine Verstaatlichung diese Mechanismen aufgegeben werden würden, da sie ja eben sehr lukrativ sind.
Eine Verstaatlichung würde auch für mich sehr unklare neue Fragen eröffnen, z.B.: Auf welcher Ebene sollen die Dienste betrieben werden? Gibt es dann ein Twitter Europa? Ein Twitter Deutschland? Könnte ich dann z.B. amerikanischen Accounts folgen?

Um etwas gegen Facebook, Twitter etc. zu machen, setze ich eher auf Aufklärung und den Leuten Alternativen zu zeigen, wie eben das Fediverse.

**Lukas Schieren:** Sollten deiner Meinung nach Menschen, die in der Politik tätig sind (MdA, MdL, MdB, MdEP), öffentliche Stellen von Kommunen, Land, Bund oder der EU ebenso wie der öffentliche rechtliche Rundfunk in Deutschland im Fediverse aktiv werden, sofern Sie es nicht schon sind und wenn ja, warum?

**Stephanie Henkel:** Ich finde bei dieser Frage eine Unterscheidung zwischen Einzelpersonen und öffentlichen Stellen wichtig.

Nicht alle Einzelpersonen haben Social Media Auftritte. Es ist schön, wenn sie oder ihr Team auch im Fedi erreichbar wären, aber wichtiger ist überhaupt eine Erreichbarkeit für möglichst viele Menschen. Wichtig sind hier meiner Meinung nach Websites ohne haufenweise Cookies und die Möglichkeit Mails zu schreiben.

Bei öffentlichen Stellen sehe ich das etwas anders. Am Beispiel Dresden wird deutlich, wie gut ein sehr aktiver Auftritt bei Twitter von Bürger:innen angenommen wird. Es fragen wirklich viele dort direkt bei der Stadt an. Ein kurzer Draht und eine Kontaktmöglichkeit, dort wo die Menschen wirklich sind, sind toll. Ich würde mir hier aber sehr eine Ergänzung in den freien Netzwerken wünschen.
Meiner Meinung nach ist die Überwindung, einen schnellen Social-Media Beitrag zu schreiben, viel niedriger, als eine förmliche Mail zu tippen oder sich umständlich ein Kontaktformular rauszusuchen.
Diese Möglichkeit sollte es auch im Fediverse geben.

Bei den Öffentlich Rechtlichen würde ich mir auch sehr wünschen, dass sie aktiver im Fediverse wären. Viele , die gerade auf junge Menschen ausgerichtet sind, setzen massiv auf Social Media und haben ganze Formate, die sich auf Soziale Medien in ihren Mittelpunkt stellen (z.B. "Ich bin Sophie Scholl", ein Instagram-Projekt und Druck, eine Online-Serie bei der Clips "in Echtzeit" auf YouTube gesendet werden, die ein Mal in der Woche zu einer Folge zusammen gefasst werden und bei der die Charaktere fiktive Instagram-Profile haben).
Diese zusätzliche Bindung an Die Datensauger finde ich sehr gefährlich und würde deshalb ein paralleles Angebot im Fediverse sehr begrüßen.

**Lukas Schieren:**: Welche Vor- und Nachteile hätte es, wenn es im Fediverse mehr politische Akteur\*innen gebe?

**Stephanie Henkel:** Als großen Vorteil sehe ich natürlich eine erhöhte Erreichbarkeit und eine zusätzliche Verbreitung von politischen Informationen. Ich bemerke es gerade auf kommunaler Ebene selbst, dass die Perspektiven auf das politische Stadtgeschehen in Dresden im Fediverse sehr begrenzt sind. An Akteur:innen aus der Parteipolitik gibt es lediglich die Piraten Dresden und die Dissidenten Fraktion, von der wir Teil sind, und ganz langsam fassen die JuSos Dresden im Fediverse Fuß. Das ist zu wenig!

Als Gefahr sehe ich, dass sich ein schrofferer, respektloser Umgangstons durch mehr Politiker:innen etablieren könnte. Vieles in der Politik ist ein Showkampf und kein ringen um gute Inhalte und Perspektiven. Und es könnte sein, dass besonders viele Beiträge geschrieben werden und sich darunter viel einseitige Werbung befindet und nicht so viel Austausch, weil einfach die Taktung von Twitter übernommen wird. Sowas hat meiner Meinung nach wenig Mehrwert.

Und an dieser Stelle müssen nochmal die lieblosen, meist nicht oder nur halbherzig nachgepflegten Twitter-Bridges und Bots erwähnt werden. Davon gibt es bereits einige, vor allem auf Mastodon, und ich empfinde sie als sehr respektlos den Lesenden gegenüber.
Eine gut nachgepflegte Bridge kann sehr gut funktionieren. Aber allen neuen Parteien und Organisationen muss klar sein, dass eine richtige Kommunikation Zeit und Arbeit bedeutet.

**Lukas Schieren:** Wie kann das Fediverse ein sicherer Hafen bleiben, wenn zunehmend auch problematische politische (Kleinst-)Parteien, wie die "Basisdemokratische Partei Deutschland", Fediverse-Dienste für ihre Agenda ausnutzen?

**Stephanie Henkel:** Es ist ganz einfach: Wir lassen die lauten Leute allein. Wer mit Hass in einen Raum ruft und keine Echo hört, wird nicht mehr weiter rufen. Wenn wir alle nicht antworten, haben solche Accounts nicht lange Freude im Fediverse. Wichtig ist auch, dass uns immer klar ist, dass bei Propaganda und Hetze klar geblockt werden kann, privat und auf unseren Servern. Aber im Prinzip läuft es ja bereits genau so, wie beschrieben.
Wenn wir also weiter nach unseren Spielregeln spielen, mache ich mir da keine großen Gedanken.

**Lukas Schieren:** Wie stehst du zu „FediBlock“?

**Stephanie Henkel:** Absolut Zwiegespalten. Ich verstehe, warum es FediBlock gibt und Leute, die es nutzen.
Ich selbst blockiere aber sehr wenige Accounts, weil ich einen gewissen Öffentlichkeits- und Erreichbarkeits-Anspruch an meinen Social-Media-Auftritt habe.
Aber nicht alle Menschen wollen oder können sich dem aussetzen bzw. erfahren sie häufiger direkte Diskriminierung, als ich. Deshalb finde ich es gut, wenn diese Leute direkt über Fediblock aussortieren können, wenn sie es wollen.

Im Bezug auf das Blockieren ausgehend von ganze Instanzen, finde ich am Wichtigsten, dass die Blockierregeln klar feststehen. Wer "alles", auch viel Hass, lesen will, sollte nicht auf chaos.social wohnen. Wer gerade viel vorsortiert haben möchte, sollte keinen Account bei mastodon.social haben. Ein offener Umgang, wie blockiert wird, beugt vielen Problemen vor.

## Information

Dieses Interview fand am 07. Juli 2022 statt.
