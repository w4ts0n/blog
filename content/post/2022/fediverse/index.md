---
title: "Fediverse - können soziale Netzwerke losgelöst von kommerziellen Interessen und im Einklang mit europäischen Gesetz funktionieren?"
author: "Lukas Schieren"
date: 2022-08-24
lastmod: 2023-06-24
categories: [JPA-Nachweis]
tags:
  [
    JPA,
    Fediverse,
    Dezentrale soziale Netzwerke,
    Mastodon,
    Pixelfed,
    PeerTube,
    Mobilizon,
  ]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
description: "Soziale Medien sind unbestritten mittlerweile ein fester und gleichwohl auch wichtiger Bestandteil der Presse- und Öffentlichkeitsarbeit von Personen des öffentlichen Lebens, öffentlichen Stellen von Kommune, Land, Bund oder der EU sowie auch von gemeinnützigen Verbänden und Vereinen oder Privatpersonen. Hierbei haben sich große US-amerikanische Technologiekonzerne wie Facebook oder Twitter über die Jahre mit ihren – zentralisierten - sozialen Netzwerken eine Monopolstellung aufgebaut. Das Fediverse möchte sich mit einem uns aus anderem Kontext bewährten Konzept den Monopolen entgegenstellen."
menu:
  about:
    name: Interview mit Stephanie Henkel von dresden.network
    title: Interview mit Stephanie Henkel von dresden.network
    url: post/2022/interview
aliases:
  - /public/post/2022/fediverse
  - /2022/08/04/fediverse
comments:
  host: social.lukas-schieren.de
  username: blog
  id: 110860289432236235
---

Soziale Medien sind unbestritten mittlerweile ein fester und gleichwohl auch wichtiger Bestandteil der Presse- und Öffentlichkeitsarbeit von Personen des öffentlichen Lebens, öffentlichen Stellen von Kommune bis EU ebenso wie von gemeinnützigen Verbänden und Vereinen oder Privatpersonen.

<!--more-->

Hierbei haben sich große US-amerikanische Technologiekonzerne wie Facebook oder Twitter über die Jahre mit ihren – zentralisierten - sozialen Netzwerken eine Monopolstellung aufgebaut. Das Fediverse möchte sich mit einem uns aus anderem Kontext bewährten Konzept den Monopolen entgegenstellen.

### Was ist unter einem zentralisierten Ansatz zu verstehen?

Unter dem zentralisierten Ansatz im Kontext sozialer Medien wird verstanden, dass ein einziger Serveranbieter die Serverinfrastruktur für einen Dienst bereitstellt. Eine Kommunikation mit anderen sozialen Netzwerken außerhalb des eigenen Konzernes (Facebook) ist nicht erwünscht.

### Was ist unter einem dezentralisierten Ansatz zu verstehen?

Unter dem dezentralen Ansatz im Kontext sozialer Medien werden voneinander komplett unabhängige Server betreibende und -infrastruktur verstanden, welche über ein gemeinsames Protokoll miteinander kommunizieren. Die Kommunikation mit anderen sozialen Netzwerken, die über das standardisierte Protokoll kommunizieren können, ist erwünscht.

Dieses Prinzip kennen die meisten in einem anderen Kontext: E-Mail. Hier wird das dezentrale Prinzip seit Jahrzehnten erfolgreich abgewendet: Es gibt verschiedene Server betreibende (bspw. posteo.de oder mailbox.org), welche E-Mail-Server anbieten, welche wiederum über standardisierte Protokolle miteinander kommunizieren können.

### Was sind die Probleme der großen zumeist US-amerikanischen sozialen Netzwerke?

Im Kern sind es drei große Probleme die die bekannten großen sozialen Netzwerke allesamt mitbringen:

- Ungesunde Diskussions- und Debattenkultur: Besonders bei Twitter ist diese Entwicklung über die Jahre häufiger zu beobachten.

- LockIn-Effekt: Durch den zentralisierten Ansatz der großen sozialen Netzwerke bedingt und der damit einhergehenden Unterbindung der Kommunikation mit anderen sozialen Netzwerken (Beispiel: Facebook <=> Twitter), möchte versucht werden, die Nutzenden möglichst lange auf ihrer Plattform zu halten.

- Ein Geschäftsmodell, welches auf dem Sammeln & Verkaufen von persönlichen Daten von (Nicht-)Nutzeden und dem Brechen von Gesetzen basiert: Der ["Cambridge Analytica"-Skandal](https://www.spiegel.de/netzwelt/web/facebook-skandal-daten-von-87-millionen-nutzern-betroffen-a-1201288.html), wo Facebook persönliche Daten an das britische Analyseunternehmen verkauft hat, zeigte eindrucksvoll, dass Facebook sich nicht um den Datenschutz (in Europa) kümmert, sondern nur mehr Profit erreichen möchte.

### Was ist das Fediverse nun genau?

Der Begriff „Fediverse“ ist zunächst einmal ein [Kofferwort aus den zwei englischen Begriffen „Federation“ und „Universe“](https://de.wikipedia.org/wiki/Fediverse), also wörtlich übersetzt etwa föderiertes Universum. Und am Beispiel des Universums lässt sich das Fediverse auch gut visualisieren.

Die verschiedenen sozialen Netzwerke können sich als verschiedene Galaxien vorgestellt werden. Innerhalb der Galaxien gibt es unzählige Planeten, welche eine komplett unabhängige Instanz eines Dienstes darstellt. Auf diesen Planeten leben Lebewesen – die Nutzenden. Bis hierhin lassen sich, mit Ausnahme der vielen unabhängigen Planeten, auch die großen bekannten sozialen Medien beschreiben. Der wesentliche Unterschied besteht nun darin, dass die Lebewesen auf den unabhängigen Planeten über Galaxien hinweg miteinander kommunizieren können.

### Was für Vor- und Nachteile hat das Fediverse?

Als ein Argument für das Fediverse wird vorgebracht, dass die Dienste im Fediverse vollständig werbe- und trackingfrei sind – das bedeutet, dass es keinerlei „gesponserten Beiträgen“ wie bei Twitter gibt. Stattdessen werden die Projekte spendenfinanziert.

Gegen das Fediverse wird die geringe Verbreitung der Dienst im Fediverse genannt. Hierbei muss jedoch gesagt werden, dass sich diese stetig steigert, besonders durch den Umstand, dass immer mehr öffentliche Stellen von Bund und Ländern auf Fediverse-Instanzen, wie social.bund.de oder bawü.social aktiv werden und ganz genaue Zahlen nicht ganz möglich sind, weil es keine zentrale Analysemöglichkeit gibt. [Schnelleinstiege in das Mastodon-Netzwerk" des Bundesbeauftragten für den Datenschutz und die Informationsfreiheit (BfDI)](https://www.bfdi.bund.de/SharedDocs/Kurzmeldungen/DE/2022/04_Schnelleinstieg-Mastodon.html) oder der [Leitfaden zum Fediverse von Digitalcourage](https://digitalcourage.de/digitale-selbstverteidigung/fediverse) helfen enorm dabei, das Fediverse generell bekannter und einfach verständlich zu machen.

Ein weites Argument für das Fediverse ist der freundlichere Umgang miteinander im Vergleich zu Facebook oder Twitter. So werden vor Beiträgen, die Menschen potenziell triggern könnten (bei politischen Themen oder mentaler Gesundheit bspw.), von der Mehrheit der Nutzenden, verantwortungsvoll eine sogenannte Inhaltswarnung gesetzt, so dass der Beitrag aktiv aufgeklappt werden muss, damit sich dessen Inhalt angeschaut werden kann.

Rechte Gruppierungen, exemplarisch rechtsextreme politische Parteien wie die Alternative für Deutschland (AfD) oder die Nationaldemokratische Partei Deutschlands (NPD, könnten das Fediverse für ihre Zwecke missbrauchen. In diesem Fall muss erwähnt werden, dass es 2018 dazu kam, dass das rechtsextreme „Gab“-Netzwerk mithilfe von Mastodon dem Fediverse beitrat, allerdings wurden Sie in einer koordinierten Aktion vieler Instanzadministrationen blockiert, was dazu führte, dass Sie von den meisten Instanzen nicht erreichbar sind.

Als noch ein weiteres Argument für das Fediverse ist angesichts der Klimakrise der [ökologische Aspekt](https://www.mdr.de/nachrichten/sachsen-anhalt/podcast-digital-leben-mastodon-fediverse-uni-magdeburg-softwerke-100.html).

Serveradministrationen von Fediverse-Instanzen können sich bewusst für Rechenzentren, die in ihrem Land stehen (bspw. Deutschland, Belgien oder Schweden) entscheiden und bspw. mit Ökostrom betrieben werden und können so aktiv zum Klimaschutz beitragen.

### Fazit

Die Idee des Fediverse ist nicht neu und dennoch scheint es so wichtig unabhängige Alternativen für die großen sozialen Netzwerke zu haben, um auch das Bewusstsein der Bevölkerung in Hinblick auf die Maschen der großen sozialen Netzwerke zu schärfen.

Außerdem ist das Konstrukt Fediverse deswegen zu spannend, weil der Grundgedanke des Internets und des Fediverse in vielen Punkten deckungsgleich sind.

Dokumentationen von [ARTE](https://www.arte.tv/de/videos/100750-000-A/unterm-radar/), der [Deutschen Welle (DW)](https://invidious.snopyta.org/watch?v=Dy8ogOaKk4Y) oder Aufbereitungen wie des [ZDF Magazin Royale in ihrer Folge vom 10. Dezember 2021](https://invidious.snopyta.org/watch?v=ALzSAC4Wl6c) zeigen eindrucksvoll, wie mächtig die großen Konzerne schon jetzt sind und was neben den datenschutzrechtlichen Problematiken noch alles problematisch an den großen Konzernen ist.

### Interviews zum Fediverse

Für diesen Blogbeitrag habe ich ein Interview mit dem Mitglied der Moderation der Mastodon-Instanz dresden.network, Stephanie Henkel, geführt. Dieses ist in Volltext über [diesen Link]({{< ref "interview" >}}) abrufbar.

Darüber hinaus durfte ich für einen Vortrag zum Fediverse im Rahmen der "ITA-Messe" Ende März den Bundesbeauftragten für den Datenschutz und die Informationsfreiheit (BfDI), Prof. Ulrich Kelber, interviewen. Dieses ist über [diesen Link](https://tube.tchncs.de/w/d38ZuevUdGVqqiJqetsFE7), welcher auf die PeerTube-Instanz von tchncs.de verweist, abrufbar. Ein Transskript zu diesem Interview ist unter [diesem Link]({{< ref "transskript-interview-bfdi" >}}) abrufbar.

### Weiterführende Links

- [Das Fediverse – das bessere soziale Netzwerk? - Verbraucherfenster Hessen](https://verbraucherfenster.hessen.de/umwelt-technik/datenschutz/das-fediverse-%E2%80%93-das-bessere-soziale-netzwerk)
- [Das Fediverse – die bessere Social-Media-Welt? - Mobilsicher](https://mobilsicher.de/ratgeber/das-fediverse-die-bessere-social-media-welt)
- [Soziales Netzwerk Mastodon: Einsamer Twitter-Ersatz für Behörden - SPIEGEL](https://www.spiegel.de/netzwelt/netzpolitik/mastodon-einsamer-twitter-ersatz-fuer-behoerden-a-f67d1c5d-6dab-4d41-9e08-ede63b57ae89)
- [Facebooks unsichtbare Datensammlung - Mobilsicher](https://mobilsicher.de/ratgeber/facebooks-unsichtbare-datensammlung)
- [Was Sie über Instagram wissen sollten - Mobilsicher](https://mobilsicher.de/ratgeber/instagram-bilder-ohne-ende)
- [Was wir über den Skandal um Facebook und Cambridge Analytica wissen [UPDATE] - Netzpolitik.org](https://netzpolitik.org/2018/cambridge-analytica-was-wir-ueber-das-groesste-datenleck-in-der-geschichte-von-facebook-wissen/)
- [Distributed social media - Mastodon & Fediverse Explained (englisches Video)](https://invidious.snopyta.org/watch?v=S57uhCQBEk0)
- [Wie Facebook Telefonie- und SMS-Daten protokolliert hat - Mobilsicher](https://mobilsicher.de/aktuelles/wie-facebook-telefonie-und-sms-daten-protokolliert-hat)
- [Twitter hat Kundendaten missbraucht - Süddeutsche Zeitung (SZ)](https://www.sueddeutsche.de/wirtschaft/twitter-elon-musk-tesla-datenschutz-missbrauch-1.5592325)

---

**HINWEIS**

Dieser Blogbeitrag wurde als **Nachweis der journalistischen Tätigkeit zum Erwerb des Jugendpresse-Ausweises (JPA)** eingereicht.

---
