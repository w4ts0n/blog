---
title: "Messebericht: Florian Messe 2024 in Dresden"
author: "Lukas Schieren"
date: 2024-11-16
lastmod: 2024-11-16
categories: [Messebericht, JPA-Nachweis]
tags:
  [
    Messebericht,
    Dresden,
    Florian,
    Feuerwehr,
    Zivilschutz,
    Kastrophenschutz,
    Fachmesse,
    JPA,
    JPA2024,
    2024,
  ]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
description: "Die FLORIAN-Messe fand vom 10. bis 12. Oktober 2024 auf mehr als 30.000 m² in den Hallen der Messe Dresden statt."
aliases:
  - /public/post/2024/florian-messe
  - /2024/10/14/florian-messe
comments:
  host: social.lukas-schieren.de
  username: blog
  id: 113490911018329482
---

Die FLORIAN-Messe fand vom 10. bis 12. Oktober 2024 auf mehr als 30.000 m² in den Hallen der Messe Dresden statt.

<!--more-->

## Allgemeines zur Messe

Die FLORIAN-Messe, eine Fachmesse für Feuerwehr, Zivilschutz und Katastrophenschutz und fand vom 10. bis 12. Oktober 2024 auf mehr als 30.000 m² in der Messe Dresden (Messering 6 01067 Dresden) zum 23. Male mit über 28.000 Besuchenden statt. Die FLORIAN wird von der ORTEC Messe & Kongress GmbH organisiert und war an den drei Messetagen jeweils in der Zeit von 09:00 Uhr bis 17:00 Uhr geöffnet.

![Pressefoto: Übersichtsfoto; Bildnachweis: creatyp/Arvid Müller](./pictures/pressefoto-1-übersichtsaufnahme.jpg)

Abbildung 1: Pressefoto: Übersichtsfoto; Bildnachweis: creatyp/Arvid Müller

![Pressefoto: Messehalle von Innen; Bildnachweis: creatyp/Arvid Müller](./pictures/pressefoto-3-messe-innen.jpg)

Abbildung 2: Pressefoto: Messehalle von Innen; Bildnachweis: creatyp/Arvid Müller

![Pressefoto: Eines der vier Außengelände; Bildnachweis: creatyp/Arvid Müller](./pictures/pressefoto-4-messe-außen.jpg)
Abbildung 3: Pressefoto: Eines der vier Außengelände; Bildnachweis: creatyp/Arvid Müller

Die Zielgruppe der Messe sind Menschen, die haupt- oder ehrenamtlich bei der Feuerwehr oder in Behörden und Organisationen mit Sicherheitsaufgaben (BOS) arbeiten, sowie Entscheider\*innnen in den vorgenannten Personengruppen.

Zu den BOS gehören neben der Polizei und der Bundesanstalt Technisches Hilfswerk (THW) noch Hilfsorganisationen wie beispielsweise das Deutsche Rote Kreuz (DRK), der Malteser Hilfsdienst (MHD) oder die Johanniter-Unfall-Hilfe (JUH).

## Schwerpunkte Einsatzstellenhygiene, Neuerung von Fahrzeugen und digitale Softwarelösungen

Der Schwerpunkt der diesjährigen FLORIAN lag auf der Hygiene an Einsatzstellen von Feuerwehr, Zivil- und Katastrophenschutzorganisationen.

Es wurden neue Fahrzeuge bzw. ihre Aufbauten, überarbeitete oder neue Werkzeuge und digitale Software vorgestellt. Außerdem stand der Gesundheitsschutz und der Arbeitsschutz im Mittelpunkt. Auf der Messe wurden dem Publikum Neuerungen bei Atemschutzgeräten, Ein- oder Mehrgasmessegeräten, Handschuhen und Helmen präsentiert.

Während meines Praktikums zum Rettungshelfer NRW habe ich den
modularen Patientenmonitor mit Defibrillator lieben gelernt.

Nun hat der weltmarktführende Hersteller Corpuls ein Trainingsgerät mit Touchdisplay entwickelt, das mit den zugehörigen Reanimationstuppen kommuniziert kann, um Einsatzszenarien zu simulieren. Ein Anwendungsfall wäre, dass bei Nichtdurchführung von notwendigen Maßnahmen der Allgemeinzustand des Patienten sich zunehmend verschlechtert.

Ich hatte den Eindruck, dass dieser Hersteller den Trend vieler Firmen von Haushaltsgeräten aufgegriffen hatte, umfassend Touchelemente zu verwenden. Im Gespräch mit einem Angestellten wurde allerdings klar: Hierbei handelte es sich um ein Trainingsgerät und ist für regelmäßige Übungen von Rettungsfachpersonal oder EH-Kursen geeignet. In der Praxis werden weiterhin richtige Druckknöpfe verwendet. Richtige Entscheidung!

![Modularer Patientenmonitor mit Defibrillator (ugs. C3) mit Touchdisplay; Bildnachweis: Lukas Schieren](./pictures/c3-training.jpg)

Abbildung 4: Modularer Patientenmonitor mit Defibrillator (ugs. C3) mit Touchdisplay; Bildnachweis: Lukas Schieren

![Mobiles Gasmessgerät; Bildnachweis: Lukas Schieren](./pictures/altair-msa.jpg)

Abbildung 5: Mobiles Gasmessgerät; Bildnachweis: Lukas Schieren

Die Hersteller von Feuerwehrfahrzeugen haben verschiedene neue Fahrzeugmodelle (z.B. Fahrzeug zur Löschung von Flugzeugen) vorgestellt. Auch die Feuerwehr der Bundeswehr hatte einen Messestand. Verschiedene Firmen, bei denen Feurwehrartikel käuflich erworben werden können, waren auf der Messe vertreten.

Gleichzeitig wurden viele digitale Softwarelösungen aus dem Bereich Alarmierung vorgestellt. Meist basieren die Dienste auf Amazon Web Services (AWS) oder Microsoft Azure Cloud.

Eine Software lief im Hintergrund auf einem Debian (Linux) als Betriebssystem.

Außerdem: Keine einzige der auf der Messe vorgestellten Softwarelösungen war bzw. ist eine freie (Open Source) Lösung. Schade!

Verschiedene Feuerwehren, wie die aus Dresden, Halle und Potsdam, haben auf der Messe ihre Fahrzeuge gezeigt.

![Mannschaftswagen der Feuerwehr Dresden; Bildnachweis: Lukas Schieren](./pictures/foto0.jpg)

Abbildung 6: Mannschaftswagen der Feuerwehr Dresden; Bildnachweis: Lukas Schieren

So präsentierte die Feuerwehr aus dem sächsischen Halle (Saale) ihren Einsatzleitwagen 1.

![Einsatzleitwagen (ELW) 1 der Feuerwehr Halle (Saale); Bildnachweis: Lukas Schieren](./pictures/foto1.jpg)

Abbildung 7: Einsatzleitwagen (ELW) 1 der Feuerwehr Halle (Saale); Bildnachweis: Lukas Schieren

Die Feuerwehr aus Potsdam hat ein besonderes Fahrzeug für den Einsatz im Katastrophenschutz: Das Hytrans Fire System (HFS; [Wikipedia](https://de.wikipedia.org/wiki/Hytrans_Fire_System)).

Der Abrollbehälter HFS wird zum Löschen von Großbränden verwendet. Die Pumpe des HFS ist im Vergleich zu denen der Bundesanstalt Technisches Hilfswerk (THW) klein. Aber: Bei größeren Bränden, wie etwa auf der Schrottinsel im Duisburger Hafen ist das HFS gut einsetzbar. Das zeigte sich beispielsweise in der ersten Folge der achten Staffel für den Westdeutschen Rundfunk (WDR) produzierten Serie „Feuer & Flamme“ bei der Feuerwehr Duisburg, wo das bei der freiwilligen Feuerwehr der Stadt Duisburg organisierte HFS zum Einsatz kam.

![Hytrans Fire System der Feuerwehr Potsdam; Bildnachweis: Lukas Schieren](./pictures/foto4.jpg)

Abbildung 8: Hytrans Fire System der Feuerwehr Potsdam; Bildnachweis: Lukas Schieren

![Übersichtsfoto von Sonderfahrzeugen; Bildnachweis: Lukas Schieren](./pictures/foto5.jpg)

Abbildung 9: Übersichtsfoto von Sonderfahrzeugen; Bildnachweis: Lukas Schieren

## Fazit

Mein persönliches Highlight war ein Foto mit dem Zugführer der Berufsfeuerwehr Bochum und gleichzeitig auch dem Botschafter der FLORIAN-Messe Marcel Becker.

![Foto mit Marcel Becker (Zugführer Feuerwehr Bochum)](./pictures/lukas-und-marcel-bf-bochum.jpg)

Abbildung 10: Foto mit Marcel Becker (Zugführer Feuerwehr Bochum); Bildnachweis eigene Aufnahme.

Für mich ist klar, dass die FLORIAN mir so viel Spaß gemacht hat, dass ich 2025 sehr gerne wiederkomme.

---

HINWEIS

Dieser Blogbeitrag wurde als **Nachweis der journalistischen Tätigkeit zum Erwerb des Jugendpresse-Ausweises (JPA)** eingereicht.

---
