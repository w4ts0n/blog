---
title: "MintApps – eine Open Source Anwendung im Bereich MINT"
author: "Lukas Schieren"
date: 2023-02-05
lastmod: 2023-06-24
categories: []
tags:
  [
    Mathematik,
    Informatik,
    Naturwissenschaften,
    Physik,
    Technik,
    MINT,
    Bildung,
    MintApps,
    JavaScript,
    HTML5,
    CSS,
    Vuejs,
    Bildung, 
    Open Source,
    FLOSS,
    FOSS,
  ]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
description: "Mathematik, Informatik, Naturwissenschaften und Technik, zusammengefasst unter MINT, sind die Schlüsselfächer der Bildung in der Zukunft. Resultierend aus dieser Wichtigkeit gibt es eine viele Zahl an Anwendungen, um diese Fächer näher zu bringen – und an einem wirke ich seit etwa 1,5 Jahren mit: Das „MintApps“-Projekt. Dieses möchte ich in diesem Blogbeitrag abbilden."
aliases:
  - /2023/02/05/mintapps
comments:
  host: social.lukas-schieren.de
  username: blog
  id: 110860305095789265
---

Mathematik, Informatik, Naturwissenschaften und Technik, zusammengefasst unter MINT, sind die Schlüsselfächer der Bildung in der Zukunft. Resultierend aus dieser Wichtigkeit gibt es eine viele Zahl an Anwendungen, um diese Fächer näher zu bringen – und an einem wirke ich seit etwa 1,5 Jahren mit: Das „MintApps“-Projekt. Dieses möchte ich in diesem Blogbeitrag abbilden.

<!--more-->

--- 
## Disclaimer

Im Sinne meines Verständnisses von Transparenz möchte ich auf folgendes eindeutig hinweisen: Ich bin seit Oktober 2021 aktiver Mitwirkender am „MintApps“-Projekt.

--- 

## Was sind die MintApps & wer hat sie ins Leben gerufen?

Im Kern versteckt sich hinter den MintApps eine Sammlung an kleinen JavaScript Anwendungen, welche Simulationen zu den Fächern Mathematik, Informatik, Naturwissenschaften und Technik bereitstellen. Darüber hinaus gibt es noch den Teil "Tools", wo Werkzeuge, wie ein QR-Code Generator oder Quizzen zu finden sind. Die Quizze dienen der spielerischen Vermittlung von naturwissenschaftlichen Zusammenhängen.

Der Mensch, welche die MintApps ins Leben gerufen hat und aktueller Maintainer ist, ist Dr. Thomas Kippenberg, Lehrkraft im Freistaat Bayern.

## Seit wann gibt es die MintApps?

Der Ursprung lässt sich schwierig genau datieren, weil die MintApps anfangs im reinen JavaScript geschrieben wurden und irgendwann stetig auf das JavaScript-Framework „vuejs“ migriert wurden. Mittlerweile sind die MintApps komplett in „vuejs“ geschrieben

Der initiale Commit in dem Codeberg-Repository unter https://codeberg.org/mintapps/client ist auf den 30. Oktober 2020 datiert.

Bild des Repository, Stand 03. Februar 2023: ![Bildbeschreibung: Client-Repository der MintApps](./images/mintapps-codeberg.JPG)

## Wie kam es zu diesen?

Die Idee zu den MintApps kam Thomas nachdem ein Laborversuch schief ging. Diesen wollte er dann digital simulieren, um seinen Schüler:innen den Versuch dennoch zeigen zu können.

Weitere Faktoren waren laut Thomas auf der einen Seite die COVID-19-Pandemie, die es nicht möglich machte, Versuche in Real vorführen zu können und auf der anderen Seite die Einführung der Quantenphysik im bayrischen Lehrplan, da die Versuche in diesem Bereich sehr teuer und komplex sind.

## Welche Apps beinhalten die MintApps aktuell? 

Eine Übersicht mit allen zum Stand der Veröffentlichung MintApps in den Bereichen
[Mathematik](https://kippenbergs.de/mint-index-math), [Informatik](https://kippenbergs.de/mint-index-info), [Tools](https://kippenbergs.de/mint-index-tools): 

![Bildbeschreibung: Übersicht der Bereichen in den MintApps](./images/mintapps-overview.JPG)

### Mathematik 

Die Mathematik-MintApps sind nochmal unter den Unterkategorien "Analysis", "Lineare Algebra", "Stochastik" und "zahlentheorie und Mengenlehre" katalogisiert.

Im ersten Segment der Mathematik-MintApps "Analysis" sind die [Lineare Gleichung](https://kippenbergs.de/mint-linear), [Quadratische Gleichung](https://kippenbergs.de/mint-quadratic) und die [Polynomfunktion](https://kippenbergs.de/mint-polynom) beheimatet.

Im zweiten Segment "Lineare Algebra" ist aktuell nur der [Gauß-Algorithmus](https://kippenbergs.de/mint-gauss) zu finden.

Im vorletzten Segment "Stochastik" ist zum Zeitpunkt des Blogbeitrags nur die [Binomialverteilung](https://kippenbergs.de/mint-binom-dist) katalogisiert.

Im letzten Segment "Zahlentheorie und Mengenlehre" sind die 
[Komplexen Zahlen](https://kippenbergs.de/mint-complex), die [Kreiszahl π](https://kippenbergs.de/mint-pi) und die [Primzahlen](https://kippenbergs.de/mint-prim) zu finden.

![Bildbeschreibung: Übersicht der MintApps im Bereich Mathematik, Stand 03. Februar 2023](./images/mintapps-mathematik.JPG)

### Informatik

In der Kategorie "Informatik" werden die MintApps nochmals in Unterkategorien eingeordnet: "Fraktale - faszinierende Selbstähnlichkeit" und "Knobeln und Rätseln"

Unter der Unterkategorie "Fraktale - faszinierende Selbstähnlichkeit" finden sich die [Kochsche Schneeflocke](https://kippenbergs.de/mint-koch), [Baum des Pythagoras](https://kippenbergs.de/mint-pythagoras), [Sierpinski-Dreieck](https://kippenbergs.de/mint-sierpinski), [Mandelbrot-Menge](https://kippenbergs.de/mint-mandelbrot) und die [Julia-Mengen](https://kippenbergs.de/mint-julia). 

![Bildbeschreibung: Übersicht der MintApps im Bereich Informatik, Stand 03. Februar 2023](./images/mintapps-informatik-1.JPG)

In der Unterkategorie "Knobeln und Rätseln" sind das [Spiel des Lebens](https://kippenbergs.de/mint-life), [Tic-Tac-Toe](https://kippenbergs.de/mint-tictac), [Vier-gewinnt](https://kippenbergs.de/mint-fourwins), [Vier-Farben-Problem](https://kippenbergs.de/mint-fourcolors), und ein [VerHEXtes Sodoku](https://kippenbergs.de/mint-sudoku) zu finden. 

![Bildbeschreibung: Übersicht der MintApps im Bereich Informatik, Stand 03. Februar 2023](./images/mintapps-informatik-2.JPG)

### Naturwissenschaften / Physik

Der naturwissenschaftliche Bereich mit dem Schwerpunkt Physik ist der umfangreichste Bereich der MintApps. 

Im ersten Bereich der Physik-MintApps sind unter "Grundlegende Mechanik" die [waagrechte Ebene](https://kippenbergs.de/mint-horizontal-plane), die [schiefe Ebene](https://kippenbergs.de/mint-tilted-plane), der [schiefer Wurf](https://kippenbergs.de/mint-throw), die [Waagrechte Rakete](https://kippenbergs.de/mint-rocket), der [zweidimensionaler Stoß](https://kippenbergs.de/mint-collision), das [ballistisches Pendel](https://kippenbergs.de/mint-ballistic) und der [Fallschirmsprung](https://kippenbergs.de/mint-skydive) zusammengefasst

![Bildbeschreibung: Übersicht der MintApps im Bereich Physik , Stand 03. Februar 2023](./images/mintapps-physik-1.JPG)

Im zweiten Bereich sind unter "Mechanische Schwingungen und Wellen" die [Wellenwanne](https://kippenbergs.de/mint-waves), die [erzwungene Schwingung](https://kippenbergs.de/mint-resonance) und das [schwingendes Seil](https://kippenbergs.de/mint-stringwave) katalogisiert. 

![Bildbeschreibung: Übersicht der MintApps im Bereich Physik, Stand 03. Februar 2023](./images/mintapps-physik-2.JPG)

Im dritten Unterbereich der Physik-MintApps sind unter "Optik" die [Lichtspektren](https://kippenbergs.de/mint-spectrum) und die [Interferenz von Licht](https://kippenbergs.de/mint-diffraction) zu finden.

![Bildbeschreibung: Übersicht der MintApps im Bereich Physik, Stand 03. Februar 2023](./images/mintapps-physik-3.JPG)

Im vierten und dem vorletzten Bereich sind unter "Elektrisches und magnetisches Feld" die [Wheatstone Brückenschaltung](https://kippenbergs.de/mint-wheatstone), die [elektrischen Feldlinien](https://kippenbergs.de/mint-fluxline), der [Lade- und Entladevorgang von Kondensatoren](https://kippenbergs.de/mint-capacity), der  [Versuch von Millikan](https://kippenbergs.de/mint-millikan), die [Induktion in der Leiterschleife](https://kippenbergs.de/mint-induction), die [Selbstinduktion Spule](https://kippenbergs.de/mint-inductance), der [elektromagnetische Schwingkreis](https://kippenbergs.de/mint-lc), der [Linearbeschleuniger](https://kippenbergs.de/mint-linacc), das [Fadenstrahlrohr](https://kippenbergs.de/mint-teltron), der [Zyklotron](https://kippenbergs.de/mint-zyklotron) und das [Massenspektrometer](https://kippenbergs.de/mint-ams) zu finden.

![Bildbeschreibung: Übersicht der MintApps im Bereich Physik, Stand 03. Februar 2023](./images/mintapps-physik-4.JPG)

Im fünften und letzten Bereich sind unter "Quanten- und Atomphysik" der [äußere Fotoeffekt](https://kippenbergs.de/mint-photoeffect), der [Compton Effekt](https://kippenbergs.de/mint-compton), das [Doppelspalt Experiment](https://kippenbergs.de/mint-dualism), das [Mach-Zehnder-Interferometer](https://kippenbergs.de/mint-machzehnder), das [Elektron als Wellenpaket](https://kippenbergs.de/mint-wavepacket), die [Schrödingergleichung](https://kippenbergs.de/mint-schroedinger), der [Franck-Hertz-Versuch](https://kippenbergs.de/mint-franckhertz) und das [Röntgenröhre Spektrum](https://kippenbergs.de/mint-xray) zu finden.

![Bildbeschreibung: Übersicht der MintApps im Bereich Physik, Stand 03. Februar 2023](./images/mintapps-physik-5.JPG)

### Tools

Der Bereich Tools in den MintApps ist wiederum in die Abschnitte "Quiz Apps", "Kleiner Helfer" und  "Sprach-Trainer" unterteilt: 

Im ersten Abschnitt "Quiz Apps" sind die [Pairs - Paare finden](https://kippenbergs.de/mint-pairs), das [MINT-Quiz](https://kippenbergs.de/mint-quiz) und [Das große Quiz](https://kippenbergs.de/mint-price) untergebracht.

Im zweiten Abschnitt "Kleiner Helfer" sind das [Messwert-Diagramm](https://kippenbergs.de/mint-plot), der [QR-Generator](https://kippenbergs.de/mint-qr), der [QR-Reader](https://kippenbergs.de/mint-qr-reader) und der [vCard-Generator](https://kippenbergs.de/mint-vcard). 

Im dritten Abschnitt "Sprach-Trainer" sind der [TeX-Trainer](https://kippenbergs.de/mint-tex) und [Markdown Trainer](https://kippenbergs.de/mint-md) zu finden.

![Bildbeschreibung: Übersicht der MintApps im Bereich Tools, Stand 03. Februar 2023](./images/mintapps-tools.JPG)


## Wie bin ich auf die MintApps aufmerksam geworden?

Auf die MintApps aufmerksam geworden bin ich durch einen Beitrag im Fediverse Etwa Anfang 2021 vom Schwester-Projekt der MintApps, den LernTools. Und diese hatten mir dann die MintApps empfohlen. 

Ich schaute mir im Anschluss an die Empfehlung die MintApps an und war begeistert. Beim Ausprobieren dieser konnte ich auch einen Fehler bei einer Simulation bemerken und diesen dem Projekt mit [diesem Issue](https://codeberg.org/mintapps/client/issues/2) mitgeben – dieser konnte aber schnell behoben werden. 

## An welchen Dingen der MintApps war und/oder bin ich aktiv beteiligt (gewesen)?

Zum einen war und bin ich die treibende Kraft hinter einer Fediverse-Präsenz (https://bildung.social/@mintapps) und betreue diesen gemeinsam mit Thomas.

![Bildbeschreibung: Fediverse-Profil der MintApps auf Bildung.social](./images/mintapps-fediverse.JPG)

Zum anderen war und bin ich ganz entschieden an der Möglichkeit der Auswahl mehrerer Sprachen außer Deutsch und dem Ausbau der Barrierefreiheit der MintApps beteiligt.

Die MintApps haben 1360 Zeichenketten, welche aktuell in 17 Sprachen, das entspricht insgesamt 61,5 %, der Übersetzungen, die  vollständig übersetzt sind.

![Bildbeschreibung: Übersicht der MintApps-Sprchen im Übersetzungstool Weblate, Stand 05. Februar 2023 ](./images/mintapps-weblate-translate-3.JPG)

Die MintApps haben zwei Komponenten: Die MintApps-Basis-Komponenten und die MintApps-MINT-Komponenten. Die Basiskomponenten enthalten alle die Inhalte, die nicht die MintApps selber betrifft und in den MINT-Komponenten entsprechend die eigentlichen MintApps.

![Bildbeschreibung: Übersicht der MintApps-Komponenten im Übersetzungstool Weblate, Stand 03. Februar 2023](./images/mintapps-weblate-translate-1.JPG)

Es gibt aktuell Übersetzungen der MintApps in Deutsch, Englisch, Französisch, Finnisch, Dänisch, Norwegisch, Schwedisch, Niederländisch, Polnisch, Chinesisch (vereinfacht), Indonesisch, Italienisch, Russisch, Spanisch, Tschechisch, Ukrainisch, Ungarisch, Zulu.

![Bildbeschreibung: Übersicht der MintApps-Sprchen im Übersetzungstool Weblate, Stand 03. Februar 2023](./images/mintapps-weblate-translate-2.JPG)

Sollte es Menschen geben, die entweder selbst oder Bekannte und Familie haben, die einer dieser Sprachen oder eine noch nicht aufgeführte Sprache sprechen können, möchte ich dazu ermutigen, die MintApps in die jeweilige Sprache zu übersetzen bzw. fertig zu übersetzen. Mitübersetzen lassen sich die MintApps über die Weblate-Instanz des Codeberg e. V. unter https://translate.codeberg.org/projects/mintapps/.

## Weitere Informationen

- https://codeberg.org/mintapps/client

- https://codeberg.org/mintapps/weblate

- https://kippenbergs.de/

