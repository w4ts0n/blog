---
title: "In eigener Sache: Wie entsteht ein Blogbeitrag & wie funktioniert der Blog technisch?"
author: "Lukas Schieren"
date: 2023-01-20
lastmod: 2023-06-24
categories: [JPA-Nachweis, In eigener Sache]
tags:
  [
    JPA,
    Blog,
    In eigener Sache,
    Technik,
    Hugo,
    CI,
  ]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
description: "Innerhalb des letzten halben Jahres hat sich am Blog technisch wie organisatorisch einiges getan. Vor diesem Hintergrund möchte ich in diesem Beitrag einen Einblick hinter die Kulissen dieses Blogs geben. Auf der einen Seite wird es um Arbeitsabläufe von der Idee bis zur Nachbereitung von Blogbeiträgen gehen, auf der anderen Seite um die technischen Dinge (im Hintergrund)."
aliases:
  - /2023/01/20/how-to-blog
comments:
  host: social.lukas-schieren.de
  username: blog
  id: 110860301327933983
---

Innerhalb des letzten halben Jahres hat sich am Blog technisch wie organisatorisch einiges getan. Vor diesem Hintergrund möchte ich in diesem Beitrag einen Einblick hinter die Kulissen dieses Blogs geben. Auf der einen Seite wird es um Arbeitsabläufe von der Idee bis zur Nachbereitung von Blogbeiträgen gehen, auf der anderen Seite um die technischen Dinge (im Hintergrund).

<!--more-->

## Vorgeschichte

Meine Idee über einen eigenen Blog zu verfügen, reicht bis in die Jahre 2018 und 2019 zurück. Nach mehreren Versuchen habe ich dann 2020 mir die eigene Domain [lukas-schieren.de](http://lukas-schieren.de) gesichert und auf der Subdomain [blog.lukas-schieren.de](http://blog.lukas-schieren.de) meinen ersten Blog eröffnet mit „Hugo“ als Websitegenerator.

Über das Fediverse bin ich auf die Möglichkeit aufmerksam geworden, eine Webseite über ein öffentliches Git-Repository zu organisieren. Beispiele hierbei sind die Free Software Foundation Europe (FSFE), ebenso wie die Internetseiten von Aral Bakan ([ar.al](http://ar.al)), Jan Wildeboer ([jan.wildeboer.net](http://jan.wildeboer.net)) oder Nicolas Lenz ([eisfunke.com](http://eisfunke.com)).

Nach vermehrten Berührungspunkten mit dem Versionsverwaltungswerkzeug git im Rahmen der Ausbildung, haben die vorgenannten Netzauftritte den Ball schlussendlich ins Rollen gebracht und ich habe mich entschieden, meinen Blog von einem gemieteten Uberspace in ein öffentliches Git Repository zu überführen.

## Planung

### Wie wähle ich das Thema für einen Blogbeitrag & welche Informationsquellen nutze ich zum Recherchieren?

Häufig entsteht die Idee für einen Blogbeitrag aus einem spannenden Artikel heraus, welchen ich entweder im Fediverse gelesen, im Polit-Podcast "Lage der Nation" gehört, in den Comedy- und Satiresendungen "ZDF Magazin Royale", „ZDF heute-show“, „extra3“ oder „die Anstalt“ gesehen habe oder ich durch Menschen aus dem Freundes- und Bekanntenkreis auf ein Thema aufmerksam gemacht wurde.

Grundsätzlich möchte ich auch unabhängig von den Blogbeiträgen zu wichtigen Themen gut informiert sein. Hier bietet sich neben dem Lesen von Beiträgen online auch das Hören von Podcasts an. Deswegen höre ich über die Android-Podcast-App „AntennaPod“ einige Podcasts, die mich zu wichtigen (politischen) Themen (fast) täglich auf dem laufenden halten. Diese sind unter anderem "Das wichtigste heute Morgen" & "Update" vom Deutschlandfunk, "Tagesschau in 100 Sekunden", "DW News Brief", „LagederNation“, „Quarks Daily“, „studioM – MONITOR“, „Synapsen – ein Wissenschaftspodcast“ und für interessantes zur Politik in dem Bundesland, in dem ich lebe, „WDR Rheinblick – der Landespolitikpodcast“.

In Anschluss an genanntes schaue ich in der Regel bei der Süddeutschen Zeitung (SZ), der Frankfurter allgemeinen Zeitung (FAZ), der ZEIT, dem SPIEGEL, beim tagesspiegel, der Tagesschau, dem Redaktionsnetzwerk Deutschland (RND), Uebermedien, der New York Times (NYT), beim „The Wall Street Journal“ (WSJ), bei der BBC, beim Zweiten Deutschen Fernsehen (ZDF) oder dem Westdeutschen Rundfunk (WDR) nach seriösen Beiträgen, um mich weitergehend zu dem Thema zu informieren.

Interessanter Fakt: Die Politsatire-Sendung „Die Anstalt“ veröffentlicht zu jeder Sendung einen Faktencheck mit Belegen zu den in den Sendung behandelten Themen und Aussagen, so dass die Zuschauenden die Sendungen auch nachprüfen können: <https://www.zdf.de/comedy/die-anstalt/fakten-im-check-der-anstalt-118.html>

### Wie werden Ideen für Blogbeiträge dokumentiert?

Wenn ich ein spannendes Thema für einen Blogbeitrag gefunden habe, dann erstelle ich ein Issue unter <https://codeberg.org/w4ts0n/blog/issues> mit einem Arbeitstitel, wie der Titel eines Blogbeitrags heißen könnte, einer kurzen Beschreibung, um was es gehen soll. Nicht selten ist der Arbeitstitel am Anfang nicht der, der am Ende in der Produktion zu finden ist, weil während des Schreibens ein passenderer Titel gefunden wurde. Außerdem werden die Issues mit Labels versehen, wie Kategorie, Status, Priorität und für wann dieser geplant ist.

## Währenddessen

### In welchen Situationen schreibe ich meine Beiträge?

Im Regelfall habe ich mir einen Extra Termin im Kalender eingetragen, an dem ich an Blogbeiträgen schreiben möchte.

Dann wird sich an den Schreibtisch gesetzt und am PC und geschrieben, geschrieben und geschrieben. Sollte ich gerade nicht am Computer sitzen (zum Beispiel wenn ich gerade mit dem Zug, der Straßenbahn oder dem Bus unterwegs bin) und mir fallen zu dem Thema noch Dinge ein, die ich entweder noch nicht (ausführlich) behandelt habe oder zu nicht passend sind, dann schreibe ich diese in meine digitalen Notizen und übertrage diese in das Dokument und ergänze diese.

## Veröffentlichung

### Was und wie prüfe ich vor der Veröffentlichung den Blogbeitrag?

Bevor Blogbeiträge final in die Produktivumgebung überführt werden, wird sich für jeden Blogbeitrag in der Bereitstellungsumgebung Abschnitt für Abschnitt noch einmal Zeit genommen und es werden die verwendeten Quellen überprüft. Außerdem wird der Blogbeitrag auf die richtige Anwendung der aktuell gültigen deutschen Rechtschreibung und Grammatik überprüft.

Im gleichen Atemzug werden Dinge wie Schlagwörter oder Einträge in Tabellen (bspw. In die für die JPA-Nachweise) nachgeschaut, ob diese richtig gesetzt wurden. Sollten bei diesen Dingen Fehler entdeckt werden, so werden diese vor Einspielung in die Produktion korrigiert.

Sollten die vorgenannten Testungen erfolgreich sein, so wird der Blogbeitrag in die Produktion eingespielt.

## Nachbereitung

### Wie verhalte ich mich, wenn sich heraus stellt, dass eine oder mehrere Information(en) im Blog sich im Nachhinein als in Teilen oder Gänze unwahr oder irreführend herausstellt?

Sollte sich im Nachhinein eine (oder mehrere) Information(en) in einem Blogbeitrag als in Teilen oder in Gänze unwahr oder irreführend herausstellen, so werde ich sobald ich davon Kenntnis habe tätig und nehme insoweit Änderungen vor, alsdass die in Teilen oder Gänze falschen oder nicht ganz richtigen Informationen entsprechend kenntlich gemacht werden und an entsprechender Stelle richtig gestellt und korrigiert werden.

Unabhängig davon werde ich im gleichen Atemzug diese Änderung in der [entsprechenden Liste auf der Transparenz-Seite](https://blog.lukas-schieren.de/transparency/#richtigstellungen-von-in-blogbeitr%c3%a4gen-erw%c3%a4hnten-thesen-behauptungen-die-sich-nachtr%c3%a4glich-als-nicht-wahr-erwiesen-haben) vermerken.

Außerdem ist jedwede Änderung am Blog jederzeit öffentlich einsehbar unter folgendem Link : [https://codeberg.org/w4ts0n/blog/commits/branch/main](https://codeberg.org/w4ts0n/blog/commits/branch/main)

### Was mache ich, wenn neue Erkenntnisse zum im Blogbeitrag behandelten Thema?

Grundsätzlich überlege ich, ob die neuen Erkenntnisse einen neuen Blogbeitrag zwingend notwendig machen, oder ob eine kennbare Notiz im älteren Blogbeitrag ausreicht.

Sollte ich mich dazu entscheiden, einen neuen Blogbeitrag zu schreiben, wird im älteren Blogbeitrag ein Vermerk positioniert, der auf den neueren Blogbeitrag hinweist.

## Wie funktioniert der Blog technisch?

### Was ist & wie funktioniert Hugo?

Hugo unterscheidet sich zu WordPress ganz entschieden in seiner Art. Denn: Beim Aufruf einer WordPress-Seite werden bei jedem Aufruf die Dateien neu, also eben dynamisch, erzeugt. Bei Hugo wiederum werden jedes Mal die selbe Datei geladen und nur bei Änderungen aktualisiert.

### Was ist und macht eine Continuous Integration (CI) & wie funktioniert die Prozessautomatisierung meines Blogs mittels Codeberg CI??

Eine Continuous Integration, kurz CI, ist erst einmal ein technisches Werkzeug, aus der Softwareentwicklung, was es ermöglicht, regelmäßig anfallende Prozesse innerhalb eines Projektes (in meinem Fall der Blog) zu automatisieren.

Codeberg greift für die Automatisierung auf das "Woodpecker CI"-Projekt zurück. Um diese für meinen Blog verfügbar zu machen, musste ich mit einem Issue im entsprechenden Repository von Codeberg (<https://codeberg.org/Codeberg-CI/request-access>) die Freischaltung für mein Profil mit Angabe von Gründen anfragen.

Im Fall meines Blogs ist die CI dafür zuständig, Funktionen oder Blogbeiträge, aus der Bereitstellungsumgebung (Q-Umgebung) die Produktivumgebung (P-Umgebung) zusammenzubauen. 

Neue Funktionen und neue Blogbeiträge werden in Testumgebungen (Ein vollständig unabhängiger extra für den jeweiligen Zweck vorgesehenen Zweig von der jeweils aktuellen Version der P-Umgebung) gebaut, dann in die Q-Umgebung überführt und schlussendlich von der CI in die P-Umgebung überführt.

Sobald die CI die Q-Umgebung erfolgreich in der P-Umgebung überführt hat, ist das Ergebnis unter [blog.lukas-schieren.de](https://blog.lukas-schieren.de) zu sehen.

Im Kern sind die ".woodpecker.yml" und "deploy.sh"-Datei von Relevanz. In diesen wird maßgeblich der CI gesagt, was Sie tun soll und worauf Sie reagieren soll.

Die Oberfläche der Codeberg CI, basierend auf Woodpecker CI sieht so aus: ![Bildbeschreibung: Bedienoberfläche der Continuous Integration (CI) von Codeberg.](./images/20230120_woodpecker_ci_codeberg-ci_ui.jpg)

Der Quellcode der Woodpecker CI ist auf [GitHub](https://github.com/woodpecker-ci) einsehbar: Wen die Codeberg CI weitergehend interessiert, kann für weitere Informationen in der offiziellen Dokumentation des Codeberg e .V. unter [folgendem Link](https://docs.codeberg.org/ci/) nachschauen.

### Wie funktioniert das Hosting einer Internetseite auf Codeberg Pages mit eigener Domain?

Der gesamte Quelltext und Inhalt des unter [blog.lukas-schieren.de](http://blog.lukas-schieren.de) zu findenden Netzauftritts ist unter <https://codeberg.org/w4ts0n/blog> zu finden.

Generelles: Mithilfe des hugo Webseitengenerator wird ein Grundgerüst gebaut, in dem ich unter dem Ordner "Content" nun alle Beiträge im .md-Format ablegen kann, falls von mir gewünscht auch in Unterordner. Das Aussehen der Webseite wird durch den Inhalt im Ordner "Themes" und der config.toml gesteuert.

Bei meinem Blog greife ich auf das "Page Bundle"-Konzept zurück. Das bedeutet das mein "content"-Ordner wie folgt strukturiert ist:  

```
content/
├── post/
|   └── 2024
|   └── 2023
│       └── how-to-blog
|           └── index.md
                └── images
        |           └── example_picture.png
|   └── 2022
|   └── 2021
```

Es gibt folgende Umgebungen:

* Die "feature" und "blog-posts"-Zweige sind für neue Funktionen und Blogbeiträge angedacht und stellen von der Produktion vollständig unabhängige Umgebungen dar. Diese Umgebungen lassen sich auch als Testumgebungen, kurz T-Umgebungen, beschreiben.
* Im "main"-Zweig liegt der Quelltext der Webseite lose herum, ist also nicht zu einer fertigen Webseite zusammengebaut. Diese Umgebung lässt sich auch als Bereitstellungsumgebung, kurz Q-Umgebung, beschreiben.
* Im "pages" Zweig liegt der Quelltext der Webseite als darstellbare Webseite, also der produktiven Webseite, welche unter blog.lukas-schieren zu finden ist. Diese Umgebung lässt sich auch als Produktionsumgebung, kurz P-Umgebung, beschreiben.

Wie oben erwähnt beinhaltet der "pages"-Zweig die fertig generierte Webseite. Codeberg Pages schaut nun beim blog Repo ob es im Repo "blog" einen Zweig namens "pages" gibt und welcher Inhalt dort zu finden ist. Sind dort Dateien zum Darstellen einer Webseite zu finden, wird diese entsprechend der Dateien dargestellt.

Die CI generiert auf Basis des "main"-Zweiges im "pages"-Zweig eine darstellbare Seite. Die CI führt, wie in der Woodpecker-Konfigurationsdatei vorher definiert, hugo automatisch aus, sobald es eine Änderung im "main"-Zweig gibt.

Weitere Informationen finden sich in der offiziellen Dokumentation des Codeberg e .V. unter [folgendem Link](https://docs.codeberg.org/codeberg-pages/).

### Wie funktioniert die Preview-Funktion für Pull Requests?

Damit das Überprüfen von Änderungen einfacher möglich ist, hatte Christoph die Idee, mittels der CI eine Preview-Version zu erzeugen. Hierfür bedurfte es Veränderungen an zwei Dateien (.woodpecker.yml und deploy.sh). Diese Änderungen hatte Christoph mit dem Commit ee226bb2ad vorgenommen und dem Blog hinzugefügt.

In anderen Worten: Der CI wurde beigebracht, dass wenn eine Pull Request gestellt wird, resultierend aus der Codebasis automatisch ein Preview erstellt werden soll. Also eine Art aktiv austestbare Bereitstellungsumgebung (Q-Umgebungg), um die Änderungen vor dem Zusammenführen in den Hauptzweig überprüfen zu können, ob es an diesen noch Korrekturen bedarf.

Wenn keine Fehler auftreten, kann die Pull Request im Anschluss in die Produktivumgebung gepatched werden kann durch einen Merge oder squash commit. So lässt sich ohne eine lokale Version einfach prüfen, ob die vorgenommenen Änderungen wie gewünscht übernommen worden oder ob es noch weitere Änderungen bedarf.

Hierbei ist Schema der Preview-URL immer gleich: [https://w4ts0n.codeberg.page/blog/@previews-nr](https://w4ts0n.codeberg.page/blog/@previews-nr)

Es ist zu beachten, dass "nr" lediglich ein Platzhalter ist, welcher immer die Nummer des Pull Requests enthält. Wenn also Pull Request Nr. 90 eingereicht wird, so dann lautet der Preview-Link: <https://w4ts0n.codeberg.page/blog/@previews-90/>

## Fazit

Als Fazit für mich ziehe ich, dass sich die Umstellung von einem Managed Webspace zu einem öffentlichen Git-Repository als eine sehr interessante Idee herausstellt und viele interessante neue Aspekte enthält.

Meinem Transparenzanspruch im Rahmen meiner journalistischen Arbeit werde ich mit der geschaffenen Möglichkeit der jederzeitigen öffentlichen Einsicht des vollumfänglichen Quelltextes, des Inhaltes, ebenso wie jeglicher Veränderungen an den vorgenannten Elementen, gerecht.

Schlussendlich ist meine Hoffnung, dass ich mit diesem ausführlichem Blogbeitrag einen spannenden Einblick in die organisatorischen wie technischen Abläufe meines Blogs geben konnte und der Blogbeitrag interessant war.

### Last but not least

Zum Schluss möchte ich mich bei den Menschen hinter Uberspace und Codeberg e. V. bedanken. Außerdem bedanken möchte ich mich bei [Christoph](https://codeberg.org/csett86) für die treibende Kraft hinter der CI für den Blog.

---

**HINWEIS**

Dieser Blogbeitrag wurde als **Nachweis der journalistischen Tätigkeit zum Erwerb des Jugendpresse-Ausweises (JPA)** eingereicht.

---