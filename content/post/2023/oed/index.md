---
title: "In eigener Sache: Wie ich auf die verrückte Idee kam einen beruflichen Weg in den öffentlichen Dienst einzuschlagen"
author: "Lukas Schieren"
date: 2023-04-06
lastmod: 2023-06-24
categories: [In eigener Sache]
tags: [öffentlicher Dienst, öD, oeD, FediBehörde, FediVerwaltung, öffentliche Verwaltung, Arbeiten in einer Behörden, Landschaftsverband Rheinland, LVR, LVR InfoKom, IT, In eigener Sache, öffentliche Hand]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
description: "In diesem Blogbeitrag möchte ich erläutern wie ich auf die verrückte Idee kam einen beruflichen Weg in den öffentlichen Dienst (öD) einzuschlagen und meine Erfahrungen mit ihm."
aliases:
  - /2023/04/06/oed
comments:
  host: social.lukas-schieren.de
  username: blog
  id: 110860311048662021
---

In diesem Blogbeitrag möchte ich erläutern wie ich auf die verrückte Idee kam einen beruflichen Weg in den öffentlichen Dienst (öD) einzuschlagen und meine Erfahrungen mit ihm.

<!--more-->

## Vorgeschichte

Zwischen 2018 und 2022 habe ich eine Ausbildung zum Informationstechnischen Assistenten (ITA) absolviert und im Rahmen dieser Ausbildung zwischen dem 2. und 3. Ausbildungsjahr beginnend ein achtwöchiges Praktikum beim Schulverwaltungsamt (SVA) der Stadtverwaltung Bochum absolviert. 

Dieses Praktikum bei einer kommunalen Behörde hat mir so viel Spaß gemacht, dass es mein Interesse weckte den öffentlichen Dienst in die engere Auswahl meiner künftigen Arbeitgeber:innen zu wählen.

## Was ist der öffentliche Dienst?

Zunächst sollten wir einmal klären, was der öffentliche Dienst, abgekürzt mit öD, überhaupt ist. Die Bundeszentrale für politische Bildung (BpB) definiert den öffentlichen Dienst folgendermaßen:

> _"[Der] öffentliche Dienst bezeichnet die Gesamtheit aller beim Bund, den Ländern, Gemeinden sowie Stiftungen, Anstalten und einer Körperschaft des öffentlichen Rechts (KdöR) beschäftigten Arbeiter, Angestellten, Beamten sowie deren Tätigkeit."_
>
> --- <cite>Bundeszentrale für politische Bildung (BpB)[^1]</cite>

[^1]: https://www.bpb.de/kurz-knapp/lexika/politiklexikon/17944/oeffentlicher-dienst/

In anderen Worten: Auf Ebene des Bundes sind Arbeitgeber\*innen des öffentlichen Dienstes die Bundespolizei, der Zoll, das Bundeskriminalamt oder die Bundesministerien und die ihrem Geschäftsbereich nachgeorderten Behörden (Bundesnetzagentur, Bundesamt für Sicherheit in der Informationstechnik z.B.), auf Ebene der Bundes*länder* sind Arbeitgeber\*innen des öffentlichen Dienstes die Landespolizei (inkl. ), die Finanzämter, die Landesämter für Verfassungsschutz oder die Landesministerien und ihrem Geschäftsbereich nachgeorderten Behörden und zuletzt auf Ebene der Kommune bzw. des Landkreises sind Arbeitgeber\*innen des öffentlichen Dienstes die Stadt- und Landkreisverwaltungen, Museen, Wasser- und Energieversorger und auch in Teilen Einrichtungen des Gesundheitswesens (Krankenhäuser).

Es gibt zwei verschiedene Möglichkeiten Beschäftigter im öffentlichen Dienst zu sein, als Tarifbeschäftigte:r oder Beamte:r. Die Tarifbeschäftigten werden nach dem Tarifvertrag des öffentlichen Dienstes (TVöD) bezahlt und Beamte erhalten eine monatliche Besoldung. 

## Pro-/Contra öffentlicher Dienst

Nun möchte ich im folgenden Abschnitt einmal über Vor- und Nachteile des öffentlichen Dienstes schreiben.

### Welche Gründe sprechen für den öffentlichen Dienst?

Jobsicherheit: Der öffentliche Dienst und seine Aufgaben werden auch durch Krisen nicht weg fallen, sondern immer allgegenwärtig sein und omnipräsent bleiben. Haben Menschen insgesamt 15 Jahre bei Arbeigeber:innen der öffentlichen Hand gearbeitet und sind über 40 Jahre alt, so sind diese quasi - bis auf eine Ausnahme - unkündbar. Die Ausnahme ist wenn der Mensch eine (schwere) Straftat begeht. 

Weniger Druck: Der Druck ist im Vergleich zur freien Wirtschaft geringer, weil kein Geld erbringen werden muss, um zu bestehen. Hierbei sei angemerkt, dass dies mit dem Weniger Druck leider nicht auf alle Bereiche (z.B. das Gesundheitswesen) des öffentlichen Dienstes zutrifft - im IT-Bereich wiederum schon.

Arbeit von Menschen für Menschen: Es ist häufig mehr oder weniger erkennbar, welche Bereiche direkt und welche indirekt der Allgemeinheit zu gute kommen. 

Flexible Arbeitszeit (FLAZ): Das starre 8-16 Uhr Arbeitszeit-Modell gehört auch in vielen Dienststellen der öffentlichen Hand der Vergangenheit an. Es gibt auch dort Möglichkeiten die Arbeitszeit unter der Beachtung formaler Vorgaben (vorgegebener Zeitraum der Arbeitszeit, wie Mo-Fr., 6-21 Uhr) flexibel gestalten zu können. Mit der COVID-Pandemie gibt es auch immer häufiger die Möglichkeit von Zuhause arbeiten zu können.

Urlaub: Menschen die im öffentlichen Dienst arbeiten, haben als Vollzeitkraft pauschal auf 30 Tage Urlaub (Vollzeit) Anspruch, bei Teilzeitkräften reduziert sich die Anzahl der Tage entsprechend der geltenden Dienstvereinbarungen.

Überstunden: Wenn sich Überstunden anhäufen sollten, gibt es die Möglichkeit diese sich entweder über den Gehaltsstreifen auszahlen zu lassen oder als Freizeit- bzw. Zeitausgleich (FZA bzw. ZA) geltend zu machen. 

Regelmäßige Gehaltserhöhung: Nach einer ersten Eingruppierung zur Einstellung steigt das Gehalt mit den Berufsjahren stetig an. Soll heißen, dass Beschäftigte, welche ein Jahr in Erfahrungsstufe 1 gearbeitet haben **automatisch** in Erfahrungsstufe 2 aufsteigen und das entsprechend bis Stufe 6 so weitergeht. Wird die Erfahrungsstufe 6 erreicht, so können Beschäftigte nur noch mittels einer höheren Entgeltgruppe (EG/A) aufsteigen. 

Wichtig: Beim Aufstieg in eine höhere Entgeltgruppe verlieren die Beschäftigten seit 2014 ihre Erfahrungsstufe nicht mehr. 

### Welche Gründe sprechen gegen den öffentlichen Dienst?

Starre Strukturen: Auch wenn sich dies stetig bessert, sind in vielen Arbeitgeber:innen der öffentlichen Hand noch (teilweise sehr) starre Strukturen vorzufinden. 

Beständig mahlende Mühlen: Viele Prozesse können sich ziehen, so zum Beispiel kann das Anzeigen von Nebentätigkeiten von Antragstellung bis finalen Bescheid etwas - auch bis zum Ende der Probezeit - dauern.

"Historisch gewachsen": Leider auch nicht selten finden sich Dinge, die "historisch gewachsen" sind. 

Gelebte Hierarchien: Zusätzlich zu starren Strukturen kommen nicht selten auch gelebte Hierarchien hinzu. Auch hier bessert sich dies hinzu zu flacheren Hierarchien.

Wichtig: Grundsätzlich ist zu beachten, dass die vorgenannten Vor- und Nachteile auf alle Arbeitgebenden im öffentlichen Dienst zutreffen.

## Welche Möglichkeiten gibt es Stellenangebote des öffentlichen Dienstes zu finden?

Nun es gibt verschiedene Möglichkeiten auf aktive Stellenangebote von öffentlichen Arbeitgeber:innen aufmerksam zu werden.

Es gibt das von der Bundesverwaltung unter der Domain https://service.bund.de betriebene Portal, wo neben Stellenangeboten auch Ausschreibungen.

![Service.bund.de](./images/service-bund-de.png)

Außerdem gibt es das vom Landesamt für Finanzen NRW betriebe Portal https://karriere.nrw.

![Karriere.nrw](./images/karriere-nrw.png)

Darüber hinaus gibt es das von der DVZ Datenverarbeitungszentrum Mecklenburg-Vorpommern GmbH betriebe Portal https://interamt.de/, worüber sich auf Stellenangebote des öffentlichen Dienstes beworben werden kann.

![Interamt](./images/interamt.png)

Gegebenenfalls betreiben die öffentlichen Verwaltungen auch eigene Bewerbungsportal, wie z.B. die Stadt Bochum:

![Bewerbungsportal der Stadt Bochum](./images/bewerbungsportal-bochum.png)

Zuletzt besteht natürlich auch die Möglichkeit falls ihr Menschen im Familien-, Freundes- und/oder Bekanntenkreis habt , die bereits im öffentlichen Dienst arbeiten, diese einfach mal zu fragen, welche Stellen bei Ihnen aktuell frei sind.

In meinem Fall bin ich auf die Stellenausschreibung über das unter einer eigenen Subdomain des Landschaftsverband Rheinland (LVR) betriebene Bewerbungsportal aufmerksam geworden, hatte mich dort registriert und darüber auch auf die Stelle beworben.

Grundsätzlich gilt: Wenn die Stelle euch fachlich interessiert: Bewerbt euch!

## Und was machst du jetzt genau im öD - und wo überhaupt? 

Nach vielen Bewerbungen bei Arbeitgeber:innen des öffentlichen Dienst (z. B. Stadtverwaltung Mülheim an der Ruhr, Stadtverwaltung Köln oder Stadtverwaltung Oberhausen) arbeite nun seit fast sieben Monaten als unbefristeter Tarifbeschäftigter beim IT-Dienstleister für den Landschaftsverband Rheinland (LVR) LVR-InfoKom am Dienstsitz Köln. 

## Fazit

Ich kann für mich sagen, dass ich mit meiner Entscheidung beruflich in den öffentlichen Dienst zu gehen sehr zufrieden bin und mich freue mich in einem angenehmen Umfeld und coolen Mensche-n bei LVR InfoKom wichtige, spannende und Projekte für die öffentlichen Verwaltungen NRWs umzusetzen. 

Oder in anderen Worten: 

> _"Cause all of me loves all of you, love your curves and all your edges all your perfect imperfections"._ 
>
> -- Liedzeilen aus dem Lied "All of Me" von John Legend

Schlussendlich gilt für mich persönlich bei meiner Arbeit folgendes Credo : "Public Money? Public Code!" Dafür habe ich ein neues Dienst-T-Shirt. ;)

![Vorderseite T-Shirt mit "Abhängig von proprietärer Qualitätssoftware?](./images/t-shirt-1.png)

![Rückseite T-Shirt mit "Tipp: Mit Open Source Software wäre dies nicht passiert! ;)"](./images/t-shirt-2.png)