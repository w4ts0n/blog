#!/bin/bash -ex

build_directory="public"
target_branch=${2:-pages}
target_directory="blog"
remote_url=${1:-git@codeberg.org:w4ts0n/blog}
additional_file=${3}

# get the deploy branch as empty repo
# then we can also use it for preview for PRs
rm -rf "${target_directory}"
mkdir "${target_directory}"
git -C "${target_directory}" init
git -C "${target_directory}" checkout -b "${target_branch}"
git -C "${target_directory}" remote add origin "${remote_url}"
# Copy build step output to repository folder
cp -a "${build_directory}"/* "${target_directory}"/
if [ ! -z "$additional_file" ]; then
    # Copy additional files if set
    cp "${build_directory}/${additional_file}" "${target_directory}"/
fi
# stage all files
git -C "${target_directory}" add --all

# commit static site files and push to target_branch of the origin
git -C "${target_directory}" commit -m "Woodpecker CI ${CI_BUILD_CREATED}"
git -C "${target_directory}" push --force origin "${target_branch}"
