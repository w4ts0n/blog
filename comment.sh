#!/bin/bash

token=${1}
owner=${2}
repo=${3}
pr_number=${4}
preview_url=${5}

curl -H "Content-Type: application/json" \
    -H "Authorization: token $token" \
    -d '{ "body": "Automated Codeberg CI: Preview at '"$preview_url"'" }' \
    https://codeberg.org/api/v1/repos/$owner/$repo/issues/$pr_number/comments
